# About the tool
Java-cfg-builder is the tool for building control flow graphs (CFG) from the arbitrary compilable java code. 
The aim of the tool is to keep the tight connection between the source code and the CFG model. It uses Abstract Syntax Tree (AST) representation of the source code as the input to avoid the high-level abstractions being compiled away during the translation to intermediate (byte) code.


## How to install
1. Download `cfg-builder.jar` from the repository or build it from sources.
2. Install [Graphviz](http://graphviz.org/download/) for output to PNG format.

## How to run the tool
Run `cfg-builde.jar` with the cmd arguments:
- Path to sources: `-s %path_to_sources%`. Mandatory parameter, could point to the folder of a precise java class.
- Class to test: `[-class %class_name%]`. Optional parameter, defines a particular class in sources for which methods CFG will be generated.
- Methods from the class above: `[-methods %method_name_1% ... %method_name_n%]`. Optional parameter, ignored if no class name provided. Useful when user needs CFG for some particular methods only.
- Output formats: `[- out %output_format_1% ... %output_format_n%]`. Optional parameter, if not specified data will be prepared in default output format (DOT).

Examples: 
``` bash
java -jar cfg-builder.jar -s examples/Examples.java
```

``` bash
java -jar cfg-builder.jar -s examples/ -class SpoonToCFGTest -methods testCtBreak testCtContinue -out DOT PNG 

```

## Supported output formats
- [DOT](https://en.wikipedia.org/wiki/DOT_(graph_description_language)), a graph description language used by many graph rendering tools ([Graphviz](https://en.wikipedia.org/wiki/Graphviz), [Gephi](https://en.wikipedia.org/wiki/Gephi), [Tulip](https://en.wikipedia.org/wiki/Tulip_(software)), etc)
- PNG
- JSON

# Questions?

Check our [Wiki](https://gitlab.inria.fr/lsafina/java-cfg-builder/-/wikis/home) or post an issue to the tracker.
