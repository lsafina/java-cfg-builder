package output

import util.GO_WITH_DEFAULT_FORMAT
import util.NO_OUTPUT_HANDLER
import org.apache.logging.log4j.LogManager.getLogger


/**
 * contains the list of possible output formats, e.g JSON, DOT, PNG
 *
 * @param name string representation of enum format name
 */
enum class OutputFormat(name: String) {
    PNG("PNG"), JSON("JSON"), DOT("DOT"), PLAIN("PLAIN")
}

/**
 * takes output format and return corresponding object of [OutputStrategy] type
 *
 * @param format output format
 */
fun formatToStrategy(format: OutputFormat) =
    when (format) {
        OutputFormat.PNG -> {
            OutputToPNG()
        }
        OutputFormat.JSON -> {
            OutputToJSON()
        }
        OutputFormat.DOT -> {
            OutputToDOT()
        }
        else -> {
            val logger = getLogger(OutputStrategy::class.java)
            logger.warn("$format: $NO_OUTPUT_HANDLER. $GO_WITH_DEFAULT_FORMAT")
            OutputToPNG()
        }
    }