package output

import cfg.CFGNode
import cfg.ControlNode
import cfg.FailNode
import cfg.FinalNode

// DOT ELEMENTS ATTRIBUTES
const val PERIPHERIES = "peripheries"
const val SHAPE = "shape"
const val LABEL = "label"
const val COLOR = "color";

//COLORS CONST
const val BLACK = "black"
const val RED = "red"

//Set dot shapes, peripheries, colors etc
fun CFGNode.resolveShape(): DOTShape = if (this is ControlNode) DOTShape.DIAMOND else DOTShape.ELLIPSE
fun CFGNode.setPeripheries(): Int = if (this is FinalNode) 2 else 1
fun CFGNode.setColor(): String = if (this is FailNode) RED else BLACK

enum class DOTShape(val shapeName: String) {
    ELLIPSE("ellipse"), DIAMOND("diamond")
}