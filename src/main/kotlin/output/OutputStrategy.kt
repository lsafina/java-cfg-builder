package output

import cfg.CFGBuiltResult
import cfg.CFGLabel
import cfg.CFGNode
import guru.nidi.graphviz.engine.Format
import guru.nidi.graphviz.engine.Graphviz
import guru.nidi.graphviz.engine.GraphvizJdkEngine
import guru.nidi.graphviz.parse.Parser
import org.jgrapht.nio.Attribute
import org.jgrapht.nio.DefaultAttribute
import org.jgrapht.nio.dot.DOTExporter
import org.jgrapht.nio.json.JSONExporter
import java.io.File
import java.io.FileWriter


/**
 * interface for all output strategies
 *
 */
interface OutputStrategy {
    /**
     * describe actions
     *
     * @param graphBuiltResultList
     */
    fun execute(graphBuiltResultList: List<CFGBuiltResult>)
}

/**
 * class that executes the strategy for translating a graph to .dot format
 */
class OutputToDOT : OutputStrategy {
    /**
     * translate a graph to .dot format
     *
     * @param graphBuiltResult a graph to translate
     */
    override fun execute(graphBuiltResultList: List<CFGBuiltResult>) {
        //set output directory
        val dir = "$OUTPUT_DIRECTORY/$DOT_OUTPUT_DIRECTORY"
        createDir(dir)

        //process each cfg result
        graphBuiltResultList.forEach { graphBuiltResult ->

            val graph = graphBuiltResult.cfg

            val exporter = DOTExporter<CFGNode, CFGLabel> { it.id.toString() }

            exporter.setVertexAttributeProvider {
                val attributeMap = HashMap<String, Attribute>()
                attributeMap[LABEL] = DefaultAttribute.createAttribute(it.nodeToDOT())
                attributeMap[SHAPE] = DefaultAttribute.createAttribute(it.resolveShape().shapeName)
                attributeMap[PERIPHERIES] = DefaultAttribute.createAttribute(it.setPeripheries())
                attributeMap[COLOR] = DefaultAttribute.createAttribute(it.setColor())
                attributeMap
            }

            exporter.setEdgeAttributeProvider {
                val attributeMap = HashMap<String, Attribute>()
                attributeMap[LABEL] = DefaultAttribute.createAttribute(it.labelToDot())
                attributeMap
            }

            val fileName = "$dir/${FILE_PREFIX}_${graphBuiltResult.className}_${graphBuiltResult.methodName}.$DOT_EXT"
            exporter.exportGraph(graph, FileWriter(fileName))
        }
    }
}

/**
 * class that executes the strategy for translating a graph to JSON format
 */
class OutputToJSON : OutputStrategy {
    /**
     * translate a graph to JSON format
     *
     * @param graphBuiltResult a graph to translate
     */
    override fun execute(graphBuiltResultList: List<CFGBuiltResult>) {
        //set output directory
        val dir = "$OUTPUT_DIRECTORY/$JSON_OUTPUT_DIRECTORY"
        createDir(dir)

        //process each cfg result
        graphBuiltResultList.forEach { graphBuiltResult ->

            val graph = graphBuiltResult.cfg

            val jsonExporter = JSONExporter<CFGNode, CFGLabel>()

            jsonExporter.setVertexAttributeProvider {
                val attributeMap = HashMap<String, Attribute>()
                attributeMap[LABEL] = DefaultAttribute.createAttribute(it.nodeToDOT())
                attributeMap[SHAPE] = DefaultAttribute.createAttribute(it.resolveShape().shapeName)
                attributeMap[PERIPHERIES] = DefaultAttribute.createAttribute(it.setPeripheries())
                attributeMap[COLOR] = DefaultAttribute.createAttribute(it.setColor())
                attributeMap
            }

            jsonExporter.setEdgeAttributeProvider {
                val attributeMap = HashMap<String, Attribute>()
                attributeMap[LABEL] = DefaultAttribute.createAttribute(it.labelToDot())
                attributeMap
            }

            val fileName = "$dir/${FILE_PREFIX}_${graphBuiltResult.className}_${graphBuiltResult.methodName}.$JSON_EXT"
            jsonExporter.exportGraph(graph, FileWriter(fileName))
        }
    }
}

/**
 * class that executes the strategy for translating a graph to .jpg format
 */
class OutputToPNG : OutputStrategy {
    /**
     * translate a graph to .png format
     *
     * @param graphBuiltResultList a graph to translate
     */
    override fun execute(graphBuiltResultList: List<CFGBuiltResult>) {
        //set output directory
        val pngDir = "$OUTPUT_DIRECTORY/$PNG_OUTPUT_DIRECTORY"
        createDir(pngDir)

        //clean dot dir and create dot files
        val dotDir = "$OUTPUT_DIRECTORY/$DOT_OUTPUT_DIRECTORY"
        val dotFileDir = File(dotDir)
        if (dotFileDir.exists()) {
            dotFileDir.delete()
        }
        OutputToDOT().execute(graphBuiltResultList)

        //generate png for each dot file
        if (dotFileDir.isDirectory)
            dotFileDir.listFiles()?.forEach { dotFile ->
                dotFile.inputStream().use { dot ->
                    val fileContent = Parser().read(dot)
                    val pngFile = File("$pngDir/${dotFile.nameWithoutExtension}.$PNG_EXT")
                    Graphviz.fromGraph(fileContent).width(700).render(Format.PNG).toFile(pngFile)
                }
            }
    }
}


/**
 * class for encapsulating the execution of different output strategies
 *
 * @property strategy output strategy
 */
internal class Context {
    private var strategy: OutputStrategy? = null

    /**
     * setting strategy to [strategy] property
     *
     * @param strategy strategy to set
     */
    fun setStrategy(strategy: OutputStrategy?) {
        this.strategy = strategy
    }

    /**
     * executing strategy that was set previously on the graph
     *
     * @param graph to execute the strategy on
     */
    fun executeStrategy(graphBuiltResultList: List<CFGBuiltResult>): Unit? {
        return strategy?.execute(graphBuiltResultList)
    }
}

const val OUTPUT_DIRECTORY = "results"
const val DOT_OUTPUT_DIRECTORY = "dot"
const val JSON_OUTPUT_DIRECTORY = "json"
const val PNG_OUTPUT_DIRECTORY = "png"

const val FILE_PREFIX = "CFG"
const val DOT_EXT = "dot"
const val JSON_EXT = "json"
const val PNG_EXT = "png"

//create dir from path
fun createDir(pathToDir: String) = File(pathToDir).mkdirs()

