package output

import cfg.CFGBuiltResult


/**
 * Generate output for cfg in formats specified in [formats]. If [formats] is empty, generate .DOT by default
 */
class OutputStrategyFactory {
    fun output(formats: List<OutputFormat>?, graphList: List<CFGBuiltResult>) {
        if (graphList.isEmpty()) return

        val context = Context()

        //if output formats do not specified generate output by default
        if (formats.isNullOrEmpty()) {
            context.setStrategy(OutputToDOT())
            context.executeStrategy(graphList)

        } else {
            formats
                .forEach { outputFormat ->
                    context.setStrategy(formatToStrategy(outputFormat))
                    context.executeStrategy(graphList)
                }
        }
    }
}

