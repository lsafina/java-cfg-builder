package cfg

import org.jgrapht.graph.DefaultDirectedGraph
import spoon.reflect.code.*
import spoon.reflect.declaration.CtElement
import spoon.reflect.reference.CtTypeReference

/**
 * CFG node interface
 * @property id node id
 */
interface CFGNode : Cloneable {
    val id: Int
    override fun toString(): String
    fun nodeToDOT(): String
}

/**
 * Start node. Only one should be present in the graph
 */
class StartNode(override val id: Int) : CFGNode {
    override fun toString() = "$id: start"
    override fun nodeToDOT() = "START"
}

/**
 * Final node
 */
class FinalNode(override val id: Int) : CFGNode {
    override fun toString() = "$id: fin"
    override fun nodeToDOT() = "FIN"
}

/**
 * Junction node. Service node which is used to connect "meaningful" nodes to each other
 */
class JunctionNode(override val id: Int, val label: JunctionLabel) : CFGNode {
    override fun toString() = "$id: $label"
    override fun nodeToDOT() = "JN"
}

/**
 * Return node. Stores return statement
 *
 * @param body return statement
 */
class ReturnNode(private val body: CtReturn<*>, override val id: Int) : CFGNode {
    override fun toString() = "$id: return (${body.returnedExpression ?: ""})"
    override fun nodeToDOT() = "return ${body.returnedExpression ?: ""}"
}

/**
 * Conditional node, stores the list of [variables] used in conditional expression
 */
class ControlNode(val variables: Set<CtElement>, override val id: Int) : CFGNode {
    override fun toString() = "$id: condition ($variables)"
    override fun nodeToDOT() = "condition ($variables)"
}

/**
 * Basic node. Stores a [body] of statement of any type
 */
class BasicNode(val body: CtStatement, override val id: Int) : CFGNode {
    override fun toString() = "$id: statement ($body)"
    override fun nodeToDOT() = "$body"
}

/**
 * Synchronized node. Stores synchronized [expression]
 */
class SynchronizedNode(val expression: CtExpression<*>, override val id: Int) : CFGNode {
    override fun toString() = "$id: synchronized ($expression)"
    override fun nodeToDOT() = "synchronized ($expression)"
}

/**
 * Node for storing data related to thrown statement
 * @param exception type of thrown exception
 * @param body body of thrown statement
 * @param isCaught was this thrown statement processed
 */
class ThrowNode(val exception: CtTypeReference<*>?, val body: CtThrow, var isCaught: Boolean, override val id: Int) : CFGNode {
    override fun toString() = "$id: throw $exception"
    override fun nodeToDOT() = "throw $exception"
}

/**
 * general class for storing nodes of types breaking the flow (break, continue, return)
 */
abstract class BreakFlowNode(override val id: Int, open val parentLoop: CtLoop?): CFGNode

/**
 * Invocation node, stores an [invocation] object (call to some method)
 */
class InvocationNode(val invocation: CtInvocation<*>, override val id: Int) : CFGNode {
    override fun toString() = "$id: call $invocation"
    override fun nodeToDOT() = "$invocation"
}

//if the break happens inside of do-while, while, for or foreach statement, we assign parent element to it, otherwise parent element is null
class BreakNode(override val id: Int, val breakStatement: CtBreak, override val parentLoop: CtLoop?) : BreakFlowNode(id, parentLoop) {
    override fun toString() = "$id: break"
    override fun nodeToDOT() = "break"
}

//if the continue happens inside of do-while, while, for or foreach statement, we assign parent element to it, otherwise parent element is null
class ContinueNode(override val id: Int, val continueStatement: CtContinue, override val parentLoop: CtLoop?) : BreakFlowNode(id, parentLoop) {
    override fun toString() = "$id: continue"
    override fun nodeToDOT() = "continue"
}

//this node is used as a sink for exception transitions
class FailNode(override val id: Int) : CFGNode {
    override fun toString() = "$id: fail"
    override fun nodeToDOT() = "FAIL"
}

/**
 * stores information required for CFG label
 */
class CFGLabel(
    val sourceId: Int, val targetId: Int, val constraint: Operator?
) {
    override fun toString() = if (constraint == null) "$sourceId->$targetId" else "$sourceId->$targetId: $constraint"
    fun labelToDot() = constraint?.toString() ?: ""
}

/**
 * enum storing labels for different junction nodes
 * used purely internaly for service needs (help with troubleshooting)
 */
enum class JunctionLabel{
    STB, //tryblock
    SCB, ECB, //catchblock
    SFB, EFB, //final block
    SDW, EDW, MDW, //dowhile
    SCD, ECD, MCD, //condition
    SWN, EWN, MWN, //while
    SSN, ESN, MSN, //switch
    SFR, EFR, MFR //for
}

/**
 * data class storing results of building cfg from a method
 *
 * @param className class name where method resides
 * @param methodName method name
 * @param cfg built cfg obj
 */
data class CFGBuiltResult(val className: String, val methodName: String, val cfg: DefaultDirectedGraph<CFGNode, CFGLabel>)


