package cfg

import spoon.reflect.code.BinaryOperatorKind
import spoon.reflect.code.CtExpression
import spoon.reflect.code.CtInvocation
import spoon.reflect.code.UnaryOperatorKind
import spoon.support.reflect.code.*

/**
 * All methods here are inherited from the parent project and will be removed in next iteration
 */


fun buildSwitchConstraint(
    switchSelector: CtExpression<*>,
    caseExpression: CtExpression<*>
): Operator {
    val left = buildConstraint(switchSelector)
    val right = buildConstraint(caseExpression)

    return BinaryOperator(left, right, BinaryOperatorKind.EQ)
}


fun buildConstraint(operator: CtExpression<*>): Operator {
    return when (operator) {
        is CtUnaryOperatorImpl<*> -> UnaryOperator(
            buildConstraint(operator.operand),
            operator.kind
        )
        is CtBinaryOperatorImpl<*> -> BinaryOperator(
            buildConstraint(operator.leftHandOperand),
            buildConstraint(operator.rightHandOperand),
            operator.kind
        )

        is CtFieldReadImpl -> {
            val name = operator.variable.simpleName
            val variableType = VariableTypes.FIELD
            val value = operator.toString()

            VariableOperator(name, operator, variableType, value)
        }
        is CtLiteralImpl -> {
            if (operator.value!=null) {
                val opValue = operator.value.toString()
                when (operator.type.simpleName) {
                    "int" -> {
                        VariableOperator(opValue, operator, VariableTypes.LITERAL, opValue)
                    }
                    "string" -> {
                        VariableOperator(opValue, operator, VariableTypes.LITERAL, opValue)
                    }
                    "boolean", "bool" -> {
                        VariableOperator(opValue, operator, VariableTypes.LOCAL_VARIABLE, opValue)
                    }
                    else -> throw NotImplementedError()
                }
            } else {
                //TODO this is wrong
                VariableOperator("null", operator, VariableTypes.LITERAL, "null")
            }
        }
        is CtVariableReadImpl -> {
            val name = operator.variable.simpleName
            val variableType = VariableTypes.LOCAL_VARIABLE
            val value = operator.toString()

            VariableOperator(name, operator, variableType, value)
        }

        is CtInvocation<*> -> {
            return VariableOperator(operator.toString(), operator, VariableTypes.INVOCATION, null)
        }

        else -> {
            throw NotImplementedError()
        }
    }
}

fun negateConstraint(constraint: Operator): Operator {
    return when (constraint) {
        is UnaryOperator -> {
            if (constraint.kind == UnaryOperatorKind.NOT) constraint.operand
            else throw NotImplementedError()
        }
        is VariableOperator -> {
            UnaryOperator(constraint, UnaryOperatorKind.NEG)
        }
        is BinaryOperator -> {
            negateBinaryOperator(constraint)
        }
        else -> {
            throw NotImplementedError()
        }
    }
}

fun negateBinaryOperator(constraint: BinaryOperator): BinaryOperator {
    return when (constraint.kind) {
        BinaryOperatorKind.AND -> {
            BinaryOperator(
                negateConstraint(constraint.leftOperand),
                negateConstraint(constraint.rightOperand),
                BinaryOperatorKind.OR
            )
        }
        BinaryOperatorKind.OR -> {
            BinaryOperator(
                negateConstraint(constraint.leftOperand),
                negateConstraint(constraint.rightOperand),
                BinaryOperatorKind.AND
            )
        }
        BinaryOperatorKind.EQ -> BinaryOperator(
            constraint.leftOperand,
            constraint.rightOperand,
            BinaryOperatorKind.NE
        )
        BinaryOperatorKind.NE -> BinaryOperator(
            constraint.leftOperand,
            constraint.rightOperand,
            BinaryOperatorKind.EQ
        )
        BinaryOperatorKind.GT -> BinaryOperator(
            constraint.leftOperand,
            constraint.rightOperand,
            BinaryOperatorKind.LE
        )
        BinaryOperatorKind.GE -> BinaryOperator(
            constraint.leftOperand,
            constraint.rightOperand,
            BinaryOperatorKind.LT
        )
        BinaryOperatorKind.LE -> BinaryOperator(
            constraint.leftOperand,
            constraint.rightOperand,
            BinaryOperatorKind.GT
        )
        BinaryOperatorKind.LT -> BinaryOperator(
            constraint.leftOperand,
            constraint.rightOperand,
            BinaryOperatorKind.GE
        )
        else -> {
            throw NotImplementedError()
        }
    }
}