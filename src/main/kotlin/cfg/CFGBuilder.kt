package cfg

import ast.*
import org.jgrapht.graph.DefaultDirectedGraph
import spoon.reflect.code.*
import spoon.reflect.declaration.CtMethod
import spoon.reflect.reference.CtTypeReference
import spoon.reflect.visitor.filter.TypeFilter
import spoon.support.reflect.code.CtBlockImpl
import spoon.support.reflect.code.CtFieldReadImpl
import spoon.support.reflect.code.CtTypeAccessImpl
import spoon.support.reflect.code.CtVariableReadImpl
import java.rmi.UnexpectedException
import java.util.concurrent.atomic.AtomicInteger

/**
 * Builds CFG for the provided method
 *
 * Travers AST of a method and builds CFG representation for each of its elements
 *
 * @param method for which CFG is building
 */
class CFGBuilder(private val method: CtMethod<*>) {
    private val count = AtomicInteger()
    private val graph = DefaultDirectedGraph<CFGNode, CFGLabel>(CFGLabel::class.java)
    private val rootNode = StartNode(count.getAndIncrement())

    fun buildCFGStructure(): DefaultDirectedGraph<CFGNode, CFGLabel> {
        //if the method's body is not empty (e.g an abstract method)
        if (method.body != null) {
            graph.addVertex(rootNode)

            //process its body: build and connect each of its statement with the previously collected results
            val builtResult = processElementBody(
                statements = method.body.statements,
                previousResults = hashSetOf(
                    BuiltResult(startNode = rootNode, endNode = rootNode, nextReachable = true)
                )
            )
        }

        return graph
    }

    /**
     * processElementBody process one-by-one the list of statements and fold together the results of each iteration
     *
     * @param statements list of statements
     * @param previousResults previously collected results to be merged with the new ones
     * @return new results
     */
    private fun processElementBody(
        statements: List<CtStatement>,
        previousResults: HashSet<BuiltResult>
    ): HashSet<BuiltResult> = statements.fold(previousResults, ::processStatement)


    /**
     * builds a statement and merge the built results with the results collected previously
     *
     * @param previousResults results collected previously
     * @param statement statemet to build
     * @return new results
     */
    private fun processStatement(
        previousResults: HashSet<BuiltResult>,
        statement: CtStatement
    ): HashSet<BuiltResult> {
        //build the statement and merge the results
        return mergeBuiltResults(previousResults, buildStatement(statement, previousResults))
    }

    /**
     * Merges two hash sets of BuiltResult.
     * If old results contains an element that can be proceeded, this element is merged with all the elements from the new results and put in the final set.
     * The elements from the old results that can not be proceeded are put to the final set as they are.
     *
     * @param oldResults previously collected results of building a code element
     * @param newResults freshly collected results of building a code element
     */
    private fun mergeBuiltResults(
        oldResults: HashSet<BuiltResult>,
        newResults: HashSet<BuiltResult>
    ): HashSet<BuiltResult> {
        val resultSet = HashSet<BuiltResult>()

        val (previousResultsReachable, previousResultsUnreachable)
                = oldResults.partition { it.nextReachable }

        //connect end node(s) of previously collected results that can be continued, with the start node(s) of current results
        previousResultsReachable
            .forEach { pr ->
                newResults.forEach { cr ->
                    //if results are equal, we do not merge and keep one of them
                    //TODO check for other types of equality
                    if (pr == cr) {
                        resultSet.add(pr)
                    } else {

                        //connect end node of old results with the start node of new results, if it is not the same node
                        if (pr.endNode.id != cr.startNode.id)
                            graph.connectNodes(pr.endNode, cr.startNode)

                        //merge old results that can be continued with the new results
                        resultSet.add(BuiltResult(pr.startNode, cr.endNode, cr.nextReachable))
                    }
                }
            }

        //add previous results without continuation to the result set
        resultSet.addAll(previousResultsUnreachable)

        return resultSet
    }

    /**
     * takes statement and builds it or the statements of its body (for compound statements)
     *
     * @param statement current statement to be built
     * @param previousResults results collected by building of statements encountered before the current
     *
     * @return set of built results containing the start and the end nodes of the statement and the information on if the next statement is reachable
     */
    private fun buildStatement(
        statement: CtStatement,
        previousResults: HashSet<BuiltResult>
    ): HashSet<BuiltResult> {
        return when (statement) {
            is CtInvocation<*> -> buildInvocation(statement) //done
            is CtIf -> buildIf(statement) //done
            is CtBlock<*> -> buildBlock(statement, previousResults) //done
            is CtCFlowBreak -> buildFlowBreak(statement) //break, continue, return
            is CtLoop -> buildLoop(statement) //done
            is CtTry -> buildTryCatch(statement)
            is CtSwitch<*> -> buildSwitch(statement) //done
            is CtSynchronized -> buildSynchronized(statement, previousResults) //done
            is CtConditional<*> -> buildConditional(statement)
            is CtComment -> previousResults
            //is CtClass<*> -> previousResults //buildClass(statement)
            //is CtNewClass<*> -> previousResults
            else -> {
                buildBasicNode(statement)
            }
            //Types not listed above:
            // CtAssertImpl, CtAssignmentImpl, CtCodeSnippetStatementImpl, CtCommentImpl, CtLocalVariableImpl: are all processed as basic statements
            // CtCaseImpl: is processed as part of CtBreak statement
        }
    }

    /**
     * builds statements of types breaking the normal flow (return, throw, break, continue)
     *
     * @param statement breaking the flow
     * @return set of built results
     */
    private fun buildFlowBreak(statement: CtCFlowBreak): HashSet<BuiltResult> {
        return when (statement) {
            is CtReturn<*> -> buildReturn(statement) //done
            is CtThrow -> buildThrow(statement) //done
            is CtBreak -> buildBreak(statement) //done
            is CtContinue -> buildContinue(statement) //done
            //for CtLabelledFlowBreak and CtYieldStatement
            else -> throw NotImplementedError()
        }
    }

    /**
     * builds statement of loop types (for, foreach, while, do-while)
     *
     * @param statement of loop types
     * @return set of built results
     */
    private fun buildLoop(statement: CtLoop): HashSet<BuiltResult> {
        return when (statement) {
            is CtFor -> buildFor(statement)
            is CtForEach -> buildForEach(statement)
            is CtWhile -> buildWhile(statement)
            is CtDo -> buildDoWhile(statement)
            else -> throw NotImplementedError() //TODO wrong exception type
        }
    }

    /**
     * builds continue statement
     *
     * @param statement continue
     * @return set of built results
     */
    private fun buildContinue(statement: CtContinue): HashSet<BuiltResult> =
        ContinueNode(count.getAndIncrement(), statement, statement.findParentLoop()).let {
            hashSetOf(BuiltResult(it, it, false))
        }

    /**
     * builds break statement
     *
     * @param statement break
     * @return set of built results
     */
    private fun buildBreak(statement: CtBreak): HashSet<BuiltResult> =
        BreakNode(count.getAndIncrement(), statement, statement.findParentLoop()).let {
            hashSetOf(BuiltResult(it, it, false))
        }

    /**
     * builds synchronized statement
     *
     * @param statement synchronized
     * @return set of built results
     */
    private fun buildSynchronized(
        statement: CtSynchronized,
        previousResults: HashSet<BuiltResult>
    ): HashSet<BuiltResult> {
        //currently we do not implement any specific processing logic for synchronized block and threat it as ctBlock
        val node = SynchronizedNode(statement.expression, count.getAndIncrement())
        val nodeResults = hashSetOf(BuiltResult(node, node, true))
        return processElementBody(statement.block.statements, nodeResults)
    }

    /**
     * builds statements inside of code block and merge the results with the previously collected results
     *
     * @param statement code block
     * @param previousResults results collected previously
     * @return set of built results
     */
    private fun buildBlock(
        statement: CtBlock<*>,
        previousResults: HashSet<BuiltResult>
    ): HashSet<BuiltResult> {
        val blockStatements = statement.statements
        return if (blockStatements.isNotEmpty()) {
            processElementBody(statement.statements, previousResults)
        } else {
            previousResults
        }
    }

    /**
     * builds return statement
     *
     * @param statement return
     * @return set of built results
     */
    private fun buildReturn(statement: CtReturn<*>): HashSet<BuiltResult> {
        val returnNode = ReturnNode(statement, count.getAndIncrement())
        return hashSetOf(BuiltResult(returnNode, returnNode, false))
    }

    /**
     * builds "basic" statements (asserts, assignments, local variables implementations, comments and code snippets)
     *
     * @param statement basic statement
     * @return set of built results
     */
    private fun buildBasicNode(statement: CtStatement): HashSet<BuiltResult> {
        val comment = statement.comments.firstOrNull()
        if (comment != null) {
            statement.removeComment<CtComment>(comment)
        }

        val node = BasicNode(statement, count.getAndIncrement())
        return hashSetOf(BuiltResult(node, node, true))
    }

    /**
     * builds throw statement
     *
     * @param statement throw
     * @return set of built results
     */
    private fun buildThrow(statement: CtThrow): HashSet<BuiltResult> {
        val node = ThrowNode(statement.thrownExpression?.type, statement, false, count.getAndIncrement())
        return hashSetOf(BuiltResult(node, node, false))
    }

    /**
     * build invocation statement
     *
     * @param statement of CtInvocation type
     * @return results of building the element: a set where each element contains a start node, end node of the element and if the next element is reachable
     */
    private fun buildInvocation(statement: CtInvocation<*>): HashSet<BuiltResult> {
        val invocationNode = InvocationNode(statement, count.getAndIncrement())
        return hashSetOf(BuiltResult(invocationNode, invocationNode, true))
    }

    /**
     * builds the body of ctTry statement
     *
     * @param statement of CtTry type
     * @return set of built results
     */
    private fun buildTryCatch(statement: CtTry): HashSet<BuiltResult> {
        /** Sequence of actions
         * 1. Build the try block
         * 2. Build the final block
         * 3. Find and process all nodes without continuation happened inside the try block. that might be:
         * 3.1. throw nodes (not caught) happened right inside this try block, thus having no continuation
         * 3.2. throw nodes (not caught) happened deeper in the try block, thus might be already processed and have continuation (and ends with efb)
         * 3.3. invocation node with invocation with uncaught exceptions
         * 5. Nodes of type 3 shall be checked: if they throw exceptions -> check if there are catcher for them
         * 6. Nodes of type 2: if there are catcher for them find the efb to which they lead and connect efb to scb, mark throw node as caught
         */

        val resultSet = hashSetOf<BuiltResult>()

        /*** Start Building Try Block ***/
        val startTryBlock = JunctionNode(count.getAndIncrement(), JunctionLabel.STB)
        val tryResults = processElementBody(
            statement.body.statements,
            hashSetOf(BuiltResult(startTryBlock, startTryBlock, true))
        )
        /*** End Building Try Block ***/

        /*** Start Building Final Block ***/
        val (startFinalBlock, endFinalBlock) = buildFinalBlock(statement)
        /*** End Building Final Block ***/


        /*** Prepare to build catchers ***/
        //throw nodes to process (type 1-2)
        val throwNodes = graph.vertexSet()
            .filterIsInstance<ThrowNode>()
            .filter { !it.isCaught && it.body.isDescendantOf(statement) }

        //see if we can catch exceptions in throw nodes
        val (throwNodesHaveCatcher, throwNodesNoCatcher) = throwNodes.map { tn ->
            tn to statement.catchers.map { it.parameter.type }.firstOrNull { tn.exception?.isSubtypeOf(it) ?: false }
        }.partition { it.second != null }


        //invocation nodes to process (type 3)
        val invocationNodes =
            graph.vertexSet()
                .filterIsInstance<InvocationNode>()
                .filter { it.invocation.isDescendantOf(statement.body) }
                .mapNotNull {
                    val exc = it.invocation.executable?.executableDeclaration?.thrownTypes
                    if (exc != null) it to exc else null
                }

        //exceptions for which we could build catchers
        val catchableExceptions = invocationNodes.flatMap { it.second }
            .plus(throwNodesHaveCatcher.map { it.second })
            .intersect(statement.catchers.map { it.parameter.type })
        /*** END prepare build catchers ***/


        /*** Build catchers and process (1-3) nodes ***/
        val builtCatchers = statement.catchers
            .filter { it.parameter.type in catchableExceptions }
            .map { catcher ->
                //build catcher
                val (scb, ecb) = buildCatcher(catcher)

                //connect the end of a catch block with the beginning of the final block
                //sometimes catchers throw exceptions too, so we need to check, if catcher follows the normal flow
                //otherwise split it flow from the normal one
                val catcherBody = catcher.getElements(TypeFilter(CtThrow::class.java))
                if (graph.vertexSet().any { it is ThrowNode && it.body in catcherBody && !it.isCaught }){
                    val (fakeStartFinalBlock, _) = buildFinalBlock(statement)
                    graph.connectNodes(ecb, fakeStartFinalBlock)
                } else {
                    graph.connectNodes(ecb, startFinalBlock)
                }
                Triple(scb, ecb, catcher)
            }

        //connect (1-3) nodes to corresponding catcher
        builtCatchers.forEach { (scb, _, catcher) ->
            //find all corresponding throwNodes
            val (throwNodesNew, throwNodesOld) =
                throwNodesHaveCatcher
                    .filter { it.second == catcher.parameter.type }
                    .map { it.first }
                    .partition { graph.outgoingEdgesOf(it).isEmpty() }


            // throw statements, encountered directly in this ctTry: connect with scb and mark as caught
            throwNodesNew.forEach {
                graph.connectNodes(it, scb, ExceptionOperator(setOf(catcher.parameter.type)))
                it.isCaught = true
            }

            //throw statements encountered deeper in ctTry and connected to the corresponding efb: connect efb with scb, mark throw node as caught
            throwNodesOld.forEach { throwNode ->
                throwNode.isCaught = true
                graph.leadsFrom(throwNode).firstOrNull { it is JunctionNode && it.label == JunctionLabel.EFB }
                    ?.let {
                        graph.connectNodes(it, scb, ExceptionOperator(setOf(catcher.parameter.type)))
                    } ?: throw UnexpectedException("EFB node shall be present!")
            }

            //invocation statements
            invocationNodes
                .filter { (node, excSet) ->
                    excSet.any { exc -> exc.isSubtypeOf(catcher.parameter.type) }
                }
                .map { it.first }
                .forEach {
                    graph.connectNodes(it, scb, ExceptionOperator(setOf(catcher.parameter.type)))
                }
        }
        /*** END build catchers ***/

        /*** connect remaining end nodes in try block ***/

        if (throwNodesNoCatcher.isNotEmpty()) {
            val (throwNodesNoCatcherNew, throwNodesNoCatcherOld) = throwNodesNoCatcher
                .filter { !it.first.body.isDescendantOf(statement.finalizer) }
                .map { it.first }
                .partition { graph.outgoingEdgesOf(it).isEmpty() }

            //if there exist nodes with exceptions not caught, but also a good flow present, we need to create a "bad" flow with artificial final block

            throwNodesNoCatcherOld
                .mapNotNull {
                    graph.leadsFrom(it).firstOrNull { it is JunctionNode && it.label == JunctionLabel.EFB }
                }
                .forEach {
                    val (startFakeFinalBlock, endFakeFinalBlock) = buildFinalBlock(statement)
                    graph.connectNodes(it, startFakeFinalBlock)
                    resultSet.add(BuiltResult(startTryBlock, endFakeFinalBlock, false))
                }

            throwNodesNoCatcherNew.forEach {
                val (startFakeFinalBlock, endFakeFinalBlock) = buildFinalBlock(statement)
                graph.connectNodes(it, startFakeFinalBlock)
                resultSet.add(BuiltResult(startTryBlock, endFakeFinalBlock, false))
            }
        }

        //for end nodes like return
        tryResults
            .filter { it.endNode is ReturnNode || it.nextReachable }
            .forEach {
                graph.connectNodes(it.endNode, startFinalBlock)
            }

        if (throwNodes.size == throwNodesHaveCatcher.size || catchableExceptions.isNotEmpty())
            resultSet.add(BuiltResult(startTryBlock, endFinalBlock, true))

        /*** END connect remaining end nodes in try block ***/

        return resultSet
    }

    private fun buildFinalBlock(statement: CtTry): Pair<JunctionNode, JunctionNode> {
        val startFinalBlock = JunctionNode(count.getAndIncrement(), JunctionLabel.SFB)
        val finalBlockResults = processElementBody(
            statement.finalizer?.statements ?: emptyList(),
            hashSetOf(BuiltResult(startFinalBlock, startFinalBlock, true))
        )
        val endFinalBlock = JunctionNode(count.getAndIncrement(), JunctionLabel.EFB)
        finalBlockResults.forEach { graph.connectNodes(it.endNode, endFinalBlock) }
        return Pair(startFinalBlock, endFinalBlock)
    }

    private fun buildCatcher(catcher: CtCatch): Pair<JunctionNode, JunctionNode> {
        val scb = JunctionNode(count.getAndIncrement(), JunctionLabel.SCB)
        val ecb = JunctionNode(count.getAndIncrement(), JunctionLabel.ECB)
        val catcherBodyStatements = catcher.body.statements
        if (catcherBodyStatements.isNotEmpty()) {
            val results = processElementBody(catcherBodyStatements, hashSetOf(BuiltResult(scb, scb, true)))
            results.map { it.endNode }.forEach { graph.connectNodes(it, ecb) }
        } else {
            graph.connectNodes(scb, ecb)
        }
        return Pair(scb, ecb)
    }

    /**
     * find all exceptions that is possible to catch by the catchers of the provided try-catch [statement]
     *
     * @param statement try-catch statement
     * @return list of catchable exceptions
     */
    private fun findCatchableExceptions(
        statement: CtTry
    ): HashSet<CtTypeReference<*>> {
        //all catchers exceptions
        val catchersExceptions: List<CtTypeReference<*>> = statement.catchers.map { it.parameter.type }
        val catchableExceptions = hashSetOf<CtTypeReference<*>>()

        val throwNodes = graph.vertexSet()
            .filter { cfgNode -> (cfgNode as? ThrowNode)?.isCaught == false && cfgNode.body.isDescendantOf(statement) }
                as List<ThrowNode>


        //find exceptions throwing by throwNodes that are catchable by the catchers of this try-catch element
        val exceptionsThrownByThrowNodes =
            throwNodes.map { it.exception }.intersect(statement.catchers.map { it.parameter.type }).filterNotNull()
        catchableExceptions.addAll(exceptionsThrownByThrowNodes)

        //find exceptions throwing by invocations that are catchable by the catchers of this try-catch element
        val exceptionsThrownByInvocations = statement
            .body
            .getElements(TypeFilter(CtInvocation::class.java))
            .flatMap { it.executable?.executableDeclaration?.thrownTypes ?: emptySet() }
            .mapNotNull { it.findParentType(catchersExceptions) }
        catchableExceptions.addAll(exceptionsThrownByInvocations)
        return catchableExceptions
    }


    /**
     * builds do while statement
     *
     * @param statement do-while
     * @return set of built results
     */
    private fun buildDoWhile(statement: CtDo): HashSet<BuiltResult> {
        val resultSet = HashSet<BuiltResult>()
        val condition = statement.loopingExpression
        val guard = buildConstraint(condition)

        //structural nodes
        val startDoWhileNode = JunctionNode(count.getAndIncrement(), JunctionLabel.SDW)
        val controlNode = ControlNode(getConditionVariables(condition), count.getAndIncrement())
        val endDoWhileNode = JunctionNode(count.getAndIncrement(), JunctionLabel.EDW)

        val whileBodyStatements = (statement.body as CtBlockImpl<*>).statements

        //if body is empty
        if (whileBodyStatements.isEmpty()) {
            graph.connectNodes(startDoWhileNode, controlNode)
            graph.connectNodes(controlNode, startDoWhileNode, guard)
            graph.connectNodes(controlNode, endDoWhileNode, negateConstraint(guard))
            resultSet.add(BuiltResult(startDoWhileNode, endDoWhileNode, true))
        } else {
            val (canBeContinued, canNotBeContinued) =
                processElementBody(
                    whileBodyStatements,
                    hashSetOf(BuiltResult(startDoWhileNode, startDoWhileNode, true))
                )
                    .partition { it.nextReachable }

            if (canBeContinued.isNotEmpty()) {
                canBeContinued.forEach {
                    graph.connectNodes(it.endNode, controlNode)
                }

                graph.connectNodes(controlNode, startDoWhileNode, guard)
                graph.connectNodes(controlNode, endDoWhileNode, negateConstraint(guard))
                resultSet.add(BuiltResult(startDoWhileNode, endDoWhileNode, true))
            }
            //all result traces can not be continued
            else {
                resultSet.addAll(canNotBeContinued)
            }
        }

        processBreakFlowNodeInLoop<ContinueNode>(statement, resultSet, startDoWhileNode)
        processBreakFlowNodeInLoop<BreakNode>(statement, resultSet, endDoWhileNode)

        return resultSet
    }

    /**
     * build ternary conditional expression
     *
     * @param statement ternary expression
     * @return set of built results
     */
    private fun buildConditional(statement: CtConditional<*>): HashSet<BuiltResult> {
        TODO("Not yet implemented")
    }

    /**
     * builds conditional statement
     *
     * @param statement conditional
     * @return set of built results
     */
    private fun buildIf(statement: CtIf): HashSet<BuiltResult> {
        val resultSet = HashSet<BuiltResult>()
        val condition = statement.condition
        val guard = buildConstraint(condition)

        //structural nodes
        val startConditionNode = JunctionNode(count.getAndIncrement(), JunctionLabel.SCD)
        val conditionalNode = ControlNode(getConditionVariables(condition), count.getAndIncrement())
        val endConditionNode = JunctionNode(count.getAndIncrement(), JunctionLabel.ECD)
        graph.connectNodes(startConditionNode, conditionalNode)

        //service nodes
        val nodeBetweenCondAndThen = JunctionNode(count.getAndIncrement(), JunctionLabel.MCD)
        graph.connectNodes(conditionalNode, nodeBetweenCondAndThen, guard)


        //parse body of the then branch
        val thenBodyStatements = statement.getThenStatement<CtBlockImpl<*>>().statements

        val (thenResultsCanBeContinued, thenResultsCaNotBeContinued) =
            processElementBody(
                thenBodyStatements,
                hashSetOf(BuiltResult(startConditionNode, nodeBetweenCondAndThen, true))
            )
                .partition { it.nextReachable }

        //if there are some then-results with continuation, we connect their final nodes with the end node of conditional
        thenResultsCanBeContinued.forEach {
            graph.connectNodes(it.endNode, endConditionNode)
        }

        //if there are some then-results that have no continuation, we add them to the results immidiately
        resultSet.addAll(thenResultsCaNotBeContinued)


        // if the else branch exists, build its body
        val elseBody = statement.getElseStatement<CtBlockImpl<*>>()
        if (elseBody != null) {
            //create service node
            val nodeBetweenCondAndElse = JunctionNode(count.getAndIncrement(), JunctionLabel.MCD)
            graph.connectNodes(conditionalNode, nodeBetweenCondAndElse, negateConstraint(guard))

            val (elseResultsCanBeContinued, elseResultsCanNotBeContinued) =
                processElementBody(
                    elseBody.statements,
                    hashSetOf(BuiltResult(startConditionNode, nodeBetweenCondAndElse, true))
                )
                    .partition { it.nextReachable }

            //if there are some else-results that have no continuation, we add them to the results immidiately
            resultSet.addAll(elseResultsCanNotBeContinued)

            //if there are some else-results with continuation, we connect their final nodes with the end node of conditional
            elseResultsCanBeContinued.forEach {
                graph.connectNodes(it.endNode, endConditionNode)
            }
        }
        // otherwise connect the control node and the end condition node with with negated guard as the label
        else {
            graph.connectNodes(conditionalNode, endConditionNode, negateConstraint(guard))
        }

        if (graph.containsVertex(endConditionNode))
            resultSet.add(BuiltResult(startConditionNode, endConditionNode, true))

        return resultSet
    }

    /**
     * builds while statement
     *
     * @param statement while
     * @return set of built results
     */
    private fun buildWhile(statement: CtWhile): HashSet<BuiltResult> {
        val resultSet = HashSet<BuiltResult>()
        val condition = statement.loopingExpression
        val guard = buildConstraint(condition)

        //structural nodes
        val startWhileNode = JunctionNode(count.getAndIncrement(), JunctionLabel.SWN)
        val controlNode = ControlNode(getConditionVariables(condition), count.getAndIncrement())
        val intermediateNode = JunctionNode(count.getAndIncrement(), JunctionLabel.MWN)
        val endWhileNode = JunctionNode(count.getAndIncrement(), JunctionLabel.EWN)
        graph.connectNodes(startWhileNode, controlNode)
        graph.connectNodes(controlNode, intermediateNode, guard)
        graph.connectNodes(controlNode, endWhileNode, negateConstraint(guard))

        val whileBodyStatements = (statement.body as CtBlockImpl<*>).statements
        val (resultsCanBeContinued, resultsCanNotBeContinued) =
            processElementBody(whileBodyStatements, hashSetOf(BuiltResult(startWhileNode, intermediateNode, true)))
                .partition { it.nextReachable }


        //if there are some results that have no continuation, we add them to the results immediately
        resultSet.addAll(resultsCanNotBeContinued)
        if (resultsCanBeContinued.isNotEmpty()) {
            resultsCanBeContinued.forEach {
                graph.connectNodes(it.endNode, startWhileNode)
            }

            //if there are some results that have continuation, we add to the results the trace of start-end nodes
            resultSet.add(BuiltResult(startWhileNode, endWhileNode, true))
        }

        processBreakFlowNodeInLoop<ContinueNode>(statement, resultSet, startWhileNode)
        processBreakFlowNodeInLoop<BreakNode>(statement, resultSet, endWhileNode)

        return resultSet
    }

    /**
     * builds switch statement
     *
     * @param statement switch
     * @return set of built results
     */
    private fun buildSwitch(statement: CtSwitch<*>): HashSet<BuiltResult> {
        val resultSet = HashSet<BuiltResult>()
        val globalResultsCanBeContinued = HashSet<BuiltResult>()
        val switchSelector = statement.selector

        //structural nodes
        val startSwitchNode = JunctionNode(count.getAndIncrement(), JunctionLabel.SSN)
        val controlNode = ControlNode(getConditionVariables(switchSelector), count.getAndIncrement())
        val endSwitchNode = JunctionNode(count.getAndIncrement(), JunctionLabel.ESN)
        graph.connectNodes(startSwitchNode, controlNode)

        //collect constraints of switch cases and build branches
        val constraints = ArrayList<Operator>()
        statement.cases.forEach { case ->
            if (case.caseExpression != null) {
                //build constraint
                val constraint = buildSwitchConstraint(switchSelector, case.caseExpression)
                constraints.add(constraint)

                //connect conntrol node with the intermediate branch node
                val intermediateNode = JunctionNode(count.getAndIncrement(), JunctionLabel.MSN)
                graph.connectNodes(controlNode, intermediateNode, constraint)

                //build branch body
                val (resultsCanBeContinued, resultsCanNotBeContinued) = processElementBody(
                    case.statements,
                    hashSetOf(BuiltResult(startSwitchNode, intermediateNode, true))
                ).partition { it.nextReachable }

                val (endsWithBreak, endsWithSomethingElse) = resultsCanNotBeContinued.partition { it.endNode is BreakNode }
                endsWithBreak.forEach { graph.connectNodes(it.endNode, endSwitchNode) }

                //add results without continuation to result set
                resultSet.addAll(endsWithSomethingElse)
                globalResultsCanBeContinued.addAll(resultsCanBeContinued)
            }
        }

        //build else case separately
        val elseCaseConstraint = createSwitchElseGuard(constraints)
        val elseBody = statement.cases.find { it.caseExpression == null }
        if (elseBody != null) {
            val intermediateNode = JunctionNode(count.getAndIncrement(), JunctionLabel.MSN)
            graph.connectNodes(controlNode, intermediateNode, elseCaseConstraint)

            val (resultsCanBeContinued, resultsCanNotBeContinued) =
                processElementBody(
                    elseBody.statements,
                    hashSetOf(BuiltResult(startSwitchNode, intermediateNode, true))
                )
                    .partition { it.nextReachable }

            val (endsWithBreak, endsWithSomethingElse) = resultsCanNotBeContinued.partition { it.endNode is BreakNode }
            endsWithBreak.forEach { graph.connectNodes(it.endNode, endSwitchNode) }

            //add results without continuation to result set
            resultSet.addAll(endsWithSomethingElse)
            globalResultsCanBeContinued.addAll(resultsCanBeContinued)
        } else {
            graph.connectNodes(controlNode, endSwitchNode, elseCaseConstraint)
        }

        //check if there is at least one result with continuation
        if (globalResultsCanBeContinued.isNotEmpty()) {
            globalResultsCanBeContinued.forEach {
                graph.connectNodes(it.endNode, endSwitchNode)
            }

            resultSet.add(BuiltResult(startSwitchNode, endSwitchNode, true))
        }



        return resultSet
    }

    /**
     * builds for statement
     *
     * @param statement for
     * @return set of built results
     */
    private fun buildFor(statement: CtFor): HashSet<BuiltResult> {
        val guard = statement.expression?.let { buildConstraint(statement.expression)}
        val conditionalVariables = getConditionVariables(statement.expression)

        val resultSet = HashSet<BuiltResult>()
        //val guard = buildConstraint(conditionExpression, fieldName, fieldType)
        val statementBody = statement.body

        //structural nodes
        val startForNode = JunctionNode(count.getAndIncrement(), JunctionLabel.SFR)
        val initNode = BasicNode(statement.forInit.first(), count.getAndIncrement())
        val conditionNode = ControlNode(conditionalVariables, count.getAndIncrement())
        val incrementNode = BasicNode(statement.forUpdate.first(), count.getAndIncrement())
        val endForNode = JunctionNode(count.getAndIncrement(), JunctionLabel.EFR)

        //service nodes, to simplify the algorithm (will be removed during the optimization)
        val betweenInitAndConditionNode = JunctionNode(
            count.getAndIncrement(),
            JunctionLabel.MFR
        ) //to avoid building edges from increment to the conditional node, those keeping the rule, that condition node always have one incoming edge
        val betweenConditionNodeAndBody = JunctionNode(
            count.getAndIncrement(),
            JunctionLabel.MFR
        ) //to pass to build element which can not build guard
        graph.connectNodes(startForNode, initNode)
        graph.connectNodes(initNode, betweenInitAndConditionNode)
        graph.connectNodes(betweenInitAndConditionNode, conditionNode)
        graph.connectNodes(conditionNode, betweenConditionNodeAndBody, guard)
        graph.connectNodes(conditionNode, endForNode, guard?.let{negateConstraint(guard)} ?: null)

        //TODO rewrite, check if the body is empty
        val bodyStatements = (statement.body as CtBlock<*>).statements

        val (canBeContinued, canNotBeContinued) =
            processElementBody(
                bodyStatements,
                hashSetOf(BuiltResult(startForNode, betweenConditionNodeAndBody, true))
            )
                .partition { it.nextReachable }

        //we connect those for which continuation is possible with the increment node
        if (canBeContinued.isNotEmpty()) {
            //graph.connectNodes(incrementNode, betweenInitAndConditionNode)
            canBeContinued
                .forEach {
                    graph.connectNodes(it.endNode, incrementNode)
                }

            graph.connectNodes(incrementNode, betweenInitAndConditionNode)
        }

        //we believe that there always exist a trace between start for node and end for node (sfr -> init -> nodeBetweenInitAndCondition -> condition > efr)
        //and that it always might have continuation. so we add it to the final result set
        resultSet.add(BuiltResult(startForNode, endForNode, true))

        //those who can not be continued (throw, return, break nodes) are added to the final results without changes
        resultSet.addAll(canNotBeContinued)

        processBreakFlowNodeInLoop<ContinueNode>(statement, resultSet, incrementNode)
        processBreakFlowNodeInLoop<BreakNode>(statement, resultSet, endForNode)

        return resultSet
    }

    /**
     * convert foreach statement to for and build it
     *
     * @param statement foreach
     * @return set of built results
     */
    private fun buildForEach(statement: CtForEach): HashSet<BuiltResult> =
        buildFor(convertForeachToFor(statement))

    /**
     * creates guard for the else branch of switch statement as a negation of all guards of other branches
     *
     * @param guards list of guards of other branches of switch statement
     * @return a new guard
     */
    private fun createSwitchElseGuard(guards: List<Operator>): Operator? =
        when (guards.size) {
            0 -> null //if there are no other branches, thus no guards, there will be no guard for the else branch
            1 -> negateConstraint(guards.first()) //if there is only one branch and guard, the guard for the else branch is its negation
            //TODO potential bug here, guards like this is possible: "0 || 1 || null"
            else -> BinaryOperator(
                negateConstraint(guards.first()),
                createSwitchElseGuard(guards.drop(1))!!,
                BinaryOperatorKind.AND
            )
        }

    /**
     * //TODO I have no bloody idea why do we need this function
     *
     * @param condition condition statement
     * @return set of expressions
     */
    private fun getConditionVariables(condition: CtExpression<*>): Set<CtExpression<*>> =
        condition.getElements(TypeFilter(CtFieldReadImpl::class.java))
            .filter { it.target !is CtTypeAccessImpl<*> }
            .toSet()
            .union(condition.getElements(TypeFilter(CtVariableReadImpl::class.java)).toSet())

    /**
     * we use this function to connect the nodes breaking the normal flow in loops to the node from which the new iteration can be started
     *
     * @param parentLoop the loop where we are looking for the nodes breaking the flow to process
     * @param resultSet results set from where we delete the chain ending with the node breaking the flow
     */
    private inline fun <reified T : BreakFlowNode> processBreakFlowNodeInLoop(
        parentLoop: CtLoop,
        resultSet: HashSet<BuiltResult>,
        nodeToConnectTo: CFGNode
    ) =
        graph.vertexSet()
            .filter { it is T && it.parentLoop == parentLoop }
            .forEach { breakFlowNode ->
                //find and remove the trace ending with breakFlowNode from the results set, because it is now the part of the cycle starting from the start loop node and ending with the end loop node
                val nodeInResultSet = resultSet.find { it.endNode == breakFlowNode }
                resultSet.remove(nodeInResultSet)

                graph.connectNodes(breakFlowNode, nodeToConnectTo)
            }


    /**
     * This class is used to store the intermediate results of building statements and to simplify the whole process of building the CFG
     *
     * @param startNode start node of the statement or the chain of statements
     * @param endNode of the statement or the chain of statements
     * @param nextReachable shows if this statement has continuation (true for the most statements, and false for throw, break, and return statements)
     */
    data class BuiltResult(val startNode: CFGNode, val endNode: CFGNode, val nextReachable: Boolean)
}