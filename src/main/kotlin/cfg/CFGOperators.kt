package cfg

import spoon.reflect.code.BinaryOperatorKind
import spoon.reflect.code.CtExpression
import spoon.reflect.code.UnaryOperatorKind
import spoon.reflect.reference.CtTypeReference

/**
 * All methods here are inherited from the parent project and will be removed in next iteration
 */

interface Operator

class UnaryOperator(val operand: Operator, val kind: UnaryOperatorKind) :
    Operator {
    override fun toString() = "${readableKind()}$operand"

    private fun readableKind(): String {
        return when (kind) {
            UnaryOperatorKind.NOT -> "!"
            UnaryOperatorKind.NEG -> "!"
            else -> kind.toString()
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as UnaryOperator

        if (operand != other.operand) return false
        if (kind != other.kind) return false

        return true
    }

    override fun hashCode(): Int {
        var result = operand.hashCode()
        result = 31 * result + kind.hashCode()
        return result
    }
}

class BinaryOperator(
    val leftOperand: Operator,
    val rightOperand: Operator,
    val kind: BinaryOperatorKind
) : Operator {
    override fun toString() = "$leftOperand${readableKind()}$rightOperand"

    private fun readableKind(): String {
        return when (kind) {
            BinaryOperatorKind.NE -> "!="
            BinaryOperatorKind.EQ -> "=="
            BinaryOperatorKind.LE -> "<="
            BinaryOperatorKind.LT -> "<"
            BinaryOperatorKind.GE -> ">="
            BinaryOperatorKind.GT -> ">"
            BinaryOperatorKind.AND -> "&&"
            BinaryOperatorKind.OR -> "||"
            else -> kind.toString()
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as BinaryOperator

        if (leftOperand != other.leftOperand) return false
        if (rightOperand != other.rightOperand) return false
        if (kind != other.kind) return false

        return true
    }

    override fun hashCode(): Int {
        var result = leftOperand.hashCode()
        result = 31 * result + rightOperand.hashCode()
        result = 31 * result + kind.hashCode()
        return result
    }
}

class VariableOperator(
    val name: String,
    val variable: CtExpression<*>,
    val variableType: VariableTypes,
    var value: String?
) : Operator {
    override fun toString(): String = if (isEvaluated) value!! else variable.toString()
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as VariableOperator

        if (variable != other.variable) return false

        return true
    }
    override fun hashCode(): Int {
        return variable.hashCode()
    }

    var isEvaluated = false
}

class ExceptionOperator(val excSet: Set<CtTypeReference<*>>) : Operator {
    override fun toString() = excSet.toString()
}

enum class VariableTypes() {
    FIELD, FIELD_OF_INTEREST, LOCAL_VARIABLE, SIGNATURE_PARAMETER, LITERAL, INVOCATION
}
