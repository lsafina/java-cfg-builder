package cfg

import ast.leadsFrom
import org.jgrapht.graph.DefaultDirectedGraph
import java.util.concurrent.atomic.AtomicInteger

/**
 * contains various functions for optimizing the graph structure (e.g. removing orphan or junction nodes, adding final nodes)
 */
fun DefaultDirectedGraph<CFGNode, CFGLabel>.optimizeGraph(): DefaultDirectedGraph<CFGNode, CFGLabel> {
    /* mandatory actions */

    //check if there any uncaught throw statements and lead them to the fail nodes
    addFailNodes()

    //add connection to FIN node
    addFinalNodes()

    removeOrphanBranches()

    /* optional optimization */

    //remove spare junction nodes
    removeJunctionNodes()

    return this
}

/**
 * add final nodes
 */
fun DefaultDirectedGraph<CFGNode, CFGLabel>.addFailNodes() {
    val counter = AtomicInteger()
    val id = vertexSet().maxBy { it.id }?.id ?: 0
    counter.set(id + 1)

    vertexSet()
        .filter { it is ThrowNode && !it.isCaught }
        .mapNotNull { leadsFrom(it).lastOrNull() }
        .forEach { connectNodes(it, FailNode(counter.getAndIncrement())) }
}

/**
 * remove branches which does not start from the START node
 */
fun DefaultDirectedGraph<CFGNode, CFGLabel>.removeOrphanBranches() {
    val orphanStartNodes = vertexSet().filter{ this.incomingEdgesOf(it).isEmpty() && it !is StartNode }
    if (orphanStartNodes.isNotEmpty()){
        removeAllVertices(orphanStartNodes)
        removeOrphanBranches()
    }

}
