package cfg

import util.EMPTY_CLASS
import util.NO_DATA_IN_AST
import util.NO_DECLARED_CLASS
import util.NO_SUCH_METHODS
import ast.getClassByName
import org.apache.logging.log4j.LogManager
import spoon.reflect.CtModel
import spoon.reflect.code.CtComment
import spoon.reflect.declaration.CtClass
import spoon.reflect.declaration.CtMethod
import spoon.reflect.visitor.filter.TypeFilter

/**
 * CFGFactory creates CFG for the set of methods
 *
 * Methods are defined through the constructor, processed in [getASTClassMethods] and passed to [buildCFG]
 *
 * @param ast all sources ast
 * @param targetClass if specified, a class limited for building CFG
 * @param targetMethods if specified, methods in [targetClass] limited for building CFG
 */
class CFGFactory(
    private val ast: CtModel,
    private val targetClass: String?,
    private val targetMethods: List<String>
) {
    private val logger = LogManager.getLogger(this::class.java)

    /**
     * prepares AST objects of methods based on [targetClass] and [targetMethods] and build CFG for each of them
     *
     * @return list of CFG built results containing a class name, a method name and the corresponding cfg
     */
    fun run(): List<CFGBuiltResult> {
        //finds in AST methods that need to be processed
        val classToMethodsList = getASTClassMethods()
        //build CFG for each of these methods
        return buildCFG(classToMethodsList)
    }

    /**
     * finds in ast ctMethods objects corresponding to the [targetClass] and [targetMethods] if specified, otherwise takes all methods from ast
     *
     * @return list of pairs with the first element storing a class name, and the second element storing the list of class methods retrieved from ast
     */
    private fun getASTClassMethods(): List<Pair<String, List<CtMethod<*>>>> {
        //if there is no target class specified we are interested in all methods
        if (targetClass == null) {
            return ast
                .getElements(TypeFilter(CtClass::class.java))
                ?.let { ctClassesList ->
                    ctClassesList.map { ctClass ->
                        ctClass.simpleName!! to (ctClass.getElements(TypeFilter(CtMethod::class.java))
                            ?: emptyList())
                    }
                } ?: emptyList()
        }

        //if there is a target class specified, try to find it in sources
        val targetClassObj = ast.getClassByName(targetClass)
        if (targetClassObj == null) {
            logger.error("$targetClass - $NO_DECLARED_CLASS")
            return emptyList()
        }

        //collect methods in target class, either specified in [targetMethods] either all
        val targetMethodsList =
            if (targetMethods.isEmpty()) {
                val methods = targetClassObj.getElements(TypeFilter(CtMethod::class.java))
                if (methods.isEmpty())
                    logger.warn("$targetClass $EMPTY_CLASS")

                methods
            } else {
                val methods = targetClassObj.getElements(TypeFilter(CtMethod::class.java))
                    .filter { it.simpleName in targetMethods }
                if (methods.isEmpty())
                    logger.warn("$targetClass $NO_SUCH_METHODS")

                methods
            }

        return listOf(targetClass to targetMethodsList)
    }


    /**
     * builds cfg for each method of each class passed to it
     *
     * @param classToMethodsList list of pairs of a class name to a list of class' methods
     * @return list of CFG built results containing a class name, a method name and the corresponding cfg
     */
    private fun buildCFG(classToMethodsList: List<Pair<String, List<CtMethod<*>>>>): List<CFGBuiltResult> {
        //if there is no class retrieved from AST, no further step can be performed
        if (classToMethodsList.isEmpty()) {
            logger.error(NO_DATA_IN_AST)
            return emptyList()
        }

        //otherwise try to build cfg for each pair of class name and method lists
        return classToMethodsList.flatMap { (ctClass, methodsList) ->
            methodsList.map { method ->
                //remove comments from the code
                method.getElements(TypeFilter(CtComment::class.java)).forEach {
                    it.parent.removeComment<CtComment>(it)
                }

                //create cfg for each method and collect it together with method and class names
                val cfg = CFGBuilder(method).buildCFGStructure().optimizeGraph()
                CFGBuiltResult(ctClass, method.simpleName, cfg)
            }
        }
    }
}