package cfg

import org.jgrapht.graph.DefaultDirectedGraph
import ast.children
import ast.leaves
import ast.orphans
import ast.parents
import java.util.concurrent.atomic.AtomicInteger

/**
 * Collects all possible traces from the the start node to each node in CFG
 * @return Map with the key of node's id and the value of list of traces: each trace is a list of predecessors' id in the order of occurrence in CFG
 */
fun DefaultDirectedGraph<CFGNode, CFGLabel>.collectNodePredecessors(): HashMap<Int, ArrayList<ArrayList<Int>>> {
    val predecessorsTraces = HashMap<Int, ArrayList<ArrayList<Int>>>()
    this.vertexSet().forEach { node -> predecessorsTraces[node.id] = arrayListOf() }

    //collects traces for the nodes in CFG: starts from a provided node and continues recursively on its descendants
    fun collectTraces(node: CFGNode) {
        val children = this.children(node) //collects all nodes the node connects to
        for (child in children) {
            val childPredecessors = predecessorsTraces[child.id]!! //collect predecessors of a child node
            val nodePredecessors = predecessorsTraces[node.id]!! //collect predecessors of the node

            //if the node we are studying has no predecessors (it is a start node), it will be the only predecessor of its child
            if (nodePredecessors.isEmpty()) {
                childPredecessors.add(arrayListOf(node.id))
            }
            //otherwise we take each trace of predecessors (since there might be several ways to reach this node)
            else {
                nodePredecessors.forEach { array ->
                    val newTrace = array.plus(node.id) as ArrayList
                    if (!childPredecessors.contains(newTrace))
                        childPredecessors.add(newTrace)
                }
            }

            collectTraces(child)
        }
    }

    //collect traces from the very top node
    val startNode = this.vertexSet().find { it.id == 0 && it is StartNode }!!
    collectTraces(startNode)

    return predecessorsTraces
}

/**
 * finds a node in CFG which body contains the exact statement
 * @param statement to compare with CFG nodes statements
 */
fun DefaultDirectedGraph<CFGNode, CFGLabel>.find(statement: Any) =
    this.vertexSet().find { it is BasicNode && it.body === statement }

/**
 * create an edge in graph between two provided nodes
 * @param first source node
 * @param second target node
 * @param constraint edge label, empty by default
 * @return boolean result of operation
 */
fun DefaultDirectedGraph<CFGNode, CFGLabel>.connectNodes(
    first: CFGNode,
    second: CFGNode,
    constraint: Operator? = null
): Boolean {
    this.addVertex(first)
    this.addVertex(second)
    val label = CFGLabel(first.id, second.id, constraint)
    return this.addEdge(first, second, label)
}

fun DefaultDirectedGraph<CFGNode, CFGLabel>.removeOrphanNodes() = removeAllVertices(this.orphans())

/**
 * add final nodes to cfg
 */
fun DefaultDirectedGraph<CFGNode, CFGLabel>.addFinalNodes(): Int {
    //check if this is an empty method, so having only start node
    if (vertexSet().size == 1 && vertexSet().first() is StartNode){
        connectNodes(vertexSet().first(), FinalNode(1))
        return 1
    }

    //creating the atomic counter and setting it to the current graph max count + 1
    val count = AtomicInteger().apply {
        if (vertexSet().isEmpty()) set(1) else set(vertexSet().maxBy { it.id }!!.id + 1)
    }

    leaves()
        .filter { it is JunctionNode || it is BasicNode || it is InvocationNode }
        .forEach { node ->
            val finalNode = FinalNode(count.getAndIncrement())
            connectNodes(node, finalNode)
        }

    return count.get()
}

/**
 * remove from graph junction nodes (service nodes) if they are not importnant for the structure of element
 * e.g if there are several consequent junction nodes
 */
fun DefaultDirectedGraph<CFGNode, CFGLabel>.removeJunctionNodes() {
    vertexSet().filterIsInstance<JunctionNode>()
        .forEach { jn ->
            val p = parents(jn)
            val ch = children(jn)
            if (p.size == 1 && ch.size == 1) {
                val c = incomingEdgesOf(jn).first().constraint
                removeVertex(jn)
                connectNodes(p.first(), ch.first(), c)
            }
        }
}
