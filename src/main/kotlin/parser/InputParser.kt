package parser

import org.apache.logging.log4j.LogManager
import output.OutputFormat
import util.*
import java.lang.IllegalArgumentException

/**
 * contains result of parsing cmd arguments
 *
 * @param sourcesPath string path to the sources to process
 * @param targetClass class in which we will look for [targetMethods], optional argument
 * @param targetMethods set of methods to process, optional argument, ignored if className is null
 * @param outputFormats format in which output data shall be returned, chosen from the enum OutputFormat, optional argument
 */
data class InputParserResult(
    val sourcesPath: String,
    val targetClass: String? = null,
    val targetMethods: List<String>,
    val outputFormats: List<OutputFormat>? = null
)


/**
 * Implements logic for parsing cmd and keeps the logger object
 */
class InputParser {
    private val logger = LogManager.getLogger(this::class.java)
    /**
     * Takes the array of cmd parameters and parse them to [InputParserResult] data structure
     *
     * @param args array of cmd arguments
     * @return an object of [InputParserResult] type or null if parsing was unsuccessful
     */
    fun parseCmdArgs(args: Array<String>): InputParserResult? {

        // map options to positional arguments
        val map = args.fold(Pair(emptyMap<String, List<String>>(), "")) { (map, lastKey), elem ->
            if (elem.startsWith("-")) Pair(map + (elem to emptyList()), elem)
            else Pair(map + (lastKey to map.getOrDefault(lastKey, emptyList()) + elem), lastKey)
        }.first

        //find sources positional argument in parsed data, otherwise raise an error and exit
        val sourcesPath = map[SOURCES_FLAG]?.firstOrNull()
        if (sourcesPath == null) {
            logger.error(NO_SOURCES_ERROR)
            return null
        }

        //collect known positional arguments for output format from the parsed data
        val output = map[OUTPUT_FORMAT_FLAG]?.mapNotNull { outputType ->
            try {
                OutputFormat.valueOf(outputType)
            } catch (e: IllegalArgumentException) {
                logger.warn("$UNKNOWN_OUTPUT_FORMAT: $outputType")
                null
            }
        }

        //find target class positional argument in parsed data
        val targetClass = map[TARGET_CLASS_FLAG]?.firstOrNull()

        //find target positional arguments in parsed data (only if target class is specified)
        val targetMethods = if (targetClass != null) (map[TARGET_METHOD_FLAG] ?: listOf()) else listOf()

        //merge collected arguments together and return as [InputParserResult]
        return InputParserResult(sourcesPath, targetClass, targetMethods, output)

    }
}