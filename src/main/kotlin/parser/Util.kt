package parser

import output.OutputFormat
import javax.tools.ToolProvider

/**
 * check if provided sources are compilable
 */
fun areSourcesCompilable(sourcesPath: String): Boolean {
    val compiler = ToolProvider.getSystemJavaCompiler();
    val result = compiler.run(null, null, null, sourcesPath);
    return result == 0
}