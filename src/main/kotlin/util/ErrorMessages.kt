package util

const val ARGS_PARSE_FAIL = "Can not parse cmd arguments"
const val NO_SOURCES_ERROR = "Arguments do not contain the path to the sources"
const val UNKNOWN_OUTPUT_FORMAT = "Unknown output format"
const val SOURCES_NOT_COMPILABLE = "The sources provided are not compilable. Please note that it might cause the incorrect building of CFG"
const val NO_DECLARED_CLASS = "Declared class is not found in the sources"
const val NO_DATA_IN_AST = "The sources do not contain enough data to build CFG"
const val NO_SUCH_METHODS = "Class does not contain methods with the name(s) specified"
const val EMPTY_CLASS = "Class does not contain methods to test"
const val NO_OUTPUT_HANDLER = "Output handler for this format is not implemented"
const val GO_WITH_DEFAULT_FORMAT = "Proceeding with the format set by default"