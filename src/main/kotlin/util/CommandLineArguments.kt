package util

/**
 * specifies the location of sources
 * mandatory parameter
 * examples:  "-s /src/main/TestClass.java" (for working with one file only), "-s /src" (for working with directory)
 */
const val SOURCES_FLAG = "-s"

/**
 * specifies the exact class in the sources provided that need to be tested
 * optional parameter, is executed only if the value parameter [-class] is not empty
 * example: "-class TestClass.java"
 */
const val TARGET_CLASS_FLAG = "-class"

/**
 * specifies the methods in the class that need to be tested
 * optional parameter, is executed only if the value parameter [-class] is not empty
 * example: "-methods foo bar"
 */
const val TARGET_METHOD_FLAG = "-methods"

/**
 * specifies the output formats in which data will be presented
 * optional parameter, if not provided by default output will be in .DOT format
 * possible values: JSON, DOT, PNG
 * example: "-out PNG"
 */
const val OUTPUT_FORMAT_FLAG = "-out"

/**
 * General example:
 * "java cfg-builder.jar -s /src/main/ -class OutputStrategy.java -methods read build -out DOT PNG"
 */