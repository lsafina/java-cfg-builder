import ast.buildAST
import cfg.CFGFactory
import org.apache.logging.log4j.LogManager
import output.*
import parser.InputParser
import parser.areSourcesCompilable
import util.ARGS_PARSE_FAIL
import util.SOURCES_NOT_COMPILABLE
import kotlin.system.exitProcess


object Main {
    private val logger = LogManager.getLogger("MainClass")

    @JvmStatic
    fun main(args: Array<String>) {
        //parse cmd arguments extracting the path to sources, methods to process (optional) and output formats (optional)
        val result = InputParser().parseCmdArgs(args)

        //exit if cmd arguments were not parsed
        if (result == null) {
            logger.error(ARGS_PARSE_FAIL)
            exitProcess(1)
        }

        //check if sources are compilable. proceed if not, but warn that this might impact on the correctness of the results
        if (!areSourcesCompilable(result.sourcesPath)) {
            logger.warn(SOURCES_NOT_COMPILABLE)
        }

        //build ast
        val ast = buildAST(result.sourcesPath)

        //build cfg objects
        val cfgList = CFGFactory(ast, result.targetClass, result.targetMethods).run()

        //output data in formats received from the cmd of by default (jpg)
        OutputStrategyFactory().output(result.outputFormats, cfgList)
    }
}
