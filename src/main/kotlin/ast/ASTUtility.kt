package ast

import spoon.reflect.CtModel
import spoon.reflect.code.CtBlock
import spoon.reflect.code.CtLoop
import spoon.reflect.declaration.*
import spoon.reflect.factory.ModuleFactory
import spoon.reflect.reference.CtTypeReference
import spoon.reflect.visitor.filter.TypeFilter
import spoon.support.reflect.code.*
import spoon.support.reflect.declaration.CtClassImpl

/**
 * find ctClass obj in ast by its [name]
 */
fun CtModel.getClassByName(className: String): CtClass<*>? =
    this.allTypes.firstOrNull { it.simpleName == className } as? CtClass<*>

/**
 * find loop in ast to which [element] belongs or null
 */
fun CtElement.findParentLoop(): CtLoop? {
    return when (parent) {
        is CtMethod<*> -> null
        is CtLoop -> parent as CtLoop
        null -> throw IllegalStateException()
        else -> parent.findParentLoop()
    }
}

/**
 * chech if [element] is successor of [other] in ast
 */
fun CtElement?.isDescendantOf(other: CtElement?): Boolean {
    if (this == null || other == null) return false

    return when (parent){
        other -> true
        is CtClass<*> -> false
        else -> parent.isDescendantOf(other)
    }
}

/**
 * from each element in [list] find a supertype if exists
 */
fun CtTypeReference<*>.findParentType(list: List<CtTypeReference<*>>) = list.find { this.isSubtypeOf(it)}




