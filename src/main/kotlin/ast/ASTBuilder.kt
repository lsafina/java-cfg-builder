package ast

import spoon.Launcher
import spoon.reflect.CtModel

/**
 * build AST from the sources in the path [sources]
 *
 * @param sources path to the sources
 * @return built AST model
 */
fun buildAST(sources: String): CtModel {
    val launcher = Launcher()
    launcher.addInputResource(sources)
    try {
        launcher.buildModel()
    } catch (e: Exception) {
        println(e.message)
    }
    return launcher.model
}
