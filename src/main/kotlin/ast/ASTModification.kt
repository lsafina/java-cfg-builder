package ast

import spoon.reflect.code.*
import spoon.reflect.declaration.CtElement
import spoon.reflect.declaration.CtNamedElement
import spoon.reflect.declaration.CtTypedElement
import spoon.reflect.declaration.CtVariable
import spoon.reflect.reference.CtExecutableReference
import spoon.reflect.reference.CtReference
import spoon.reflect.reference.CtTypeReference
import spoon.support.reflect.code.*
import java.awt.List

/**
 * modifies original ast by replacing foreach statement with for statement
 *
 * For statement consists of init value of counter, condition imposed on this counter and counter increment (update)
 * For init value we create a random integer variable and set it value to 0
 * For condition we create an expression `initVal < Collection.size()` where initVal is init value, and Collection is the one used in foreach statement
 * For counter increment we create an expression `initVal++`
 * To the new for body we add as the first statement the assignment of a variable of the Collection type with the name mentioned in foreach statement to the current value in the collection to which points the index
 * E.g for foreach statement
 * `foreach( elem : elemList ){ ... }`
 * we create new for statement
 * `for (int cnt = 0; cnt < elemList.size; cnt++) { String elem = arrayList[cnt]; ... } '
 *
 * @param statement foreach statement
 * @return for statement
 */
fun convertForeachToFor(statement: CtForEach): CtFor {
    val factory = statement.factory
    val newFor = factory.createFor()
    val forEachParent = statement.parent

    //create and set new for init
    //TODO need to check if the counter name is already taken
    val init = createLocalVariable<Int>(statement, "cnt")
    setInitValue(init, 0)
    newFor.setForInit<CtFor>(listOf(init))

    //create and set new for update
    newFor.setForUpdate<CtFor>(listOf(createIncrementExpression(statement, init)))

    //create new for counter condition
    val condition = factory.createBinaryOperator<Boolean>()

    //create and set condition left hand operand
    condition.setLeftHandOperand<CtBinaryOperatorImpl<Boolean>>(factory.createVariableRead(init.reference, false))

    //construct right hand operand, which is in reality invocation to size() on of foreach collection
    val inv = factory.createInvocation<CtElement>()
    inv.setTarget<CtTargetedExpression<CtElement, CtExpression<*>>>(statement.expression) //set target collection
    val er = factory.createExecutableReference<CtElement>() //set size() call
    er.setSimpleName<CtReference>("size")
    val type = when (val se = statement.expression) {
        is CtVariableRead -> se.variable.type
        is CtFieldRead -> se.type
        //TODO remove this dirty hack
        else -> se.type
    }

    er.setDeclaringType<CtExecutableReference<CtElement>>(type)
    inv.setExecutable<CtInvocation<CtElement>>(er)

    //set condition right hand operand
    condition.setRightHandOperand<CtBinaryOperator<Boolean>>(inv)

    //set condition operation kind
    condition.setKind<CtBinaryOperator<Boolean>>(BinaryOperatorKind.LT)

    //set new for counter condition
    newFor.setExpression<CtFor>(condition)

    //set new for parent
    newFor.setParent(forEachParent)

    //creating a statement assigning to a variable current array element
    val varArrayIndex = factory.createVariableAssignment(statement.variable.reference, false, factory.createCodeSnippetExpression("${statement.expression}.get(${init.simpleName})"))

    //inserting statement to the new for body
    newFor.setBody<CtBodyHolder>(statement.body.insertBefore(varArrayIndex))
    varArrayIndex.insertBefore<CtLocalVariable<Any>>(statement.variable)

    createAssignment(statement, init)


    val v = forEachParent.factory.createLocalVariable<Any>()
    v.setSimpleName<CtNamedElement>("hi")


    //replacing foreach with new for statement
    //TODO check before cast
    (forEachParent as CtBlockImpl<*>).removeStatement(statement) // forEachParent.statements.last())
    forEachParent.addStatement<CtStatementList>(newFor)

    return newFor
}

fun createAssignment(statement: CtForEach, init: CtLocalVariable<Int>) {
    val factory = statement.factory
    //val ass = factory.createAssignment<CtLocalVariable<Object>,CtInvocation<*>>()

    //left side - arrayElem
    val arrayElem = createLocalVariable<Object>(statement, statement.variable.simpleName)
    val type = factory.createTypeReference<Object>()
    type.setSimpleName<CtReference>("Object")
    val packref = factory.createPackageReference()
    packref.setSimpleName<CtReference>("java.lang")
    type.setPackage<CtTypeReference<Object>>(packref)
    arrayElem.setType<CtTypedElement<Object>>(type)


    ///right side l.get(i)
    val inv = factory.createInvocation<Object>()
    val varRead = factory.createVariableRead<Int>()
    varRead.setType<CtTypedElement<Int>>(factory.createTypeReference())
    varRead.setVariable<CtVariableAccess<Int>>(init.reference)
    inv.addArgument<CtAbstractInvocation<Object>>(varRead)

    val exref = factory.createExecutableReference<List>()
    exref.setSimpleName<CtReference>("get")

    //ass.setAssigned<>(arrayElem)
}

fun setInitValue(variable: CtLocalVariable<Int>, initVal: Int) {
    val indexInitVal = variable.factory.createLiteral(initVal)
    variable.setDefaultExpression<CtVariable<Int>>(indexInitVal)
}

private fun createIncrementExpression(
    statement: CtForEach,
    index: CtLocalVariable<Int>
): CtUnaryOperator<Int> {
    val increment = statement.factory.createUnaryOperator<Int>()
    val readVariable = statement.factory.createVariableRead(index.reference, false)
    increment.setOperand<CtUnaryOperatorImpl<*>>(readVariable)
    increment.setKind<CtUnaryOperatorImpl<*>>(UnaryOperatorKind.POSTINC)
    increment.setParent(statement)
    return increment
}

private fun <T> createLocalVariable(statement: CtForEach, name: String): CtLocalVariable<T> {
    val variable = statement.factory.createLocalVariable<T>()
    variable.setParent(statement)
    variable.setSimpleName<CtNamedElement>(name)
    return variable
}