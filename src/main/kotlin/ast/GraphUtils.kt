package ast

import org.jgrapht.graph.DefaultDirectedGraph

/**
 * get nodes in [graph] that has no input and output edges (orphans)
 */
fun <V, E> DefaultDirectedGraph<V, E>.orphans() = this.vertexSet().filter { v -> this.incomingEdgesOf(v).isEmpty() && this.outgoingEdgesOf(v).isEmpty() }

/**
 * get nodes in [graph] that has input but no output edges (leaves)
 */
fun <V, E> DefaultDirectedGraph<V, E>.leaves() = this.vertexSet().filter { v -> this.outgoingEdgesOf(v).isEmpty() && this.incomingEdgesOf(v).isNotEmpty() }

/**
 * get nodes in [graph] that are directly connected to the [node] (parents)
 *
 * @param node for which we are looking for parents
 */
fun <V, E> DefaultDirectedGraph<V, E>.parents(node: V) = this.incomingEdgesOf(node).map { e -> this.getEdgeSource(e) }
fun <V, E> DefaultDirectedGraph<V, E>.children(node: V) = this.outgoingEdgesOf(node).map { e -> this.getEdgeTarget(e) }

/**
 * finds the leaves to which [node] is connected or node itself
 * e.g for the graph with transitions (a->b, b->c, b->d), graph.leadsFrom(a) will return (c, d) and graph.leadsFrom(c) will return (c).
 */
fun <V, E> DefaultDirectedGraph<V, E>.leadsFrom(node: V): Set<V> {
    val edges = this.outgoingEdgesOf(node)

    return if (edges.isEmpty()){
        setOf(node)
    } else {
        edges.flatMap { edge -> this.leadsFrom(this.getEdgeTarget(edge)) }.toSet()
    }
}
