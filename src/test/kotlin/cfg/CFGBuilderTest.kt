package cfg

import ast.buildAST
import org.apache.logging.log4j.LogManager
import org.hamcrest.CoreMatchers
import org.junit.Rule
import org.junit.Test
import org.junit.rules.ErrorCollector
import spoon.reflect.declaration.CtMethod
import spoon.reflect.visitor.filter.TypeFilter
import spoon.support.reflect.code.CtCommentImpl

/**
 * Class for collecting and running CFG related tests
 *
 * Takes all files with tests from the `src/test/resources/` library.
 * Each file contains functions for with CFG will be built.
 * The actual result of building the CFG is compared with the expected one,
 * which is stated in a first comment of the tested function.
 */
class CFGBuilderTest {
    private val logger = LogManager.getLogger(this::class.java)

    //tests list
    private val tests = arrayListOf(
        "src/test/resources/TestExceptionHierarchy.java",
        "src/test/resources/SpoonToCFGTest.java",
        "src/test/resources/ServiceSupportTest.java")

    //error collector. stores results of failed tests
    @get:Rule
    val collector = ErrorCollector()

    /**
     * Builds cfg structure for various methods and compare the result with the expected one
     *
     * For each of the classes stored in [tests], build AST, and then collect methods in AST representation
     * For each method build CFG and compare the results with the expected one which is stored as the first comment of each tested function
     */
    @Test
    fun buildCFGStructure() {
        tests.forEach { test ->
            val ast = buildAST(test)
            val methods = ast.getElements(TypeFilter(CtMethod::class.java))

            for (method in methods) {
                val expected = method.getElements(TypeFilter(CtCommentImpl::class.java)).firstOrNull()?.content
                if (expected != null) {
                    val actual = CFGBuilder(method).buildCFGStructure().toString()
                    collector.checkThat(actual, CoreMatchers.`is`(expected))
                } else {
                    logger.warn("Test for ${method.simpleName} was ignored, because the expected result is not specified")
                }
            }
        }
    }
}