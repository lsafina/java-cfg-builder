import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;

public class SpoonToCFGTest {
    void testCtArrayRead(){
        int[] array = new int[10];
        System.out.println(
                array[0] // <-- array read
        );
    }

    void testCtArrayWrite(){
        Object[] array = new Object[10];
        // array write
        array[0] = "new value";
    }

    void testCtAssert(){
        assert 1+1==2;
    }

    void testCtAssignment(){
        int x;
        x = 4; // <-- an assignment
    }

    void testCtBinaryOperator() {
        // 3+4 is the binary expression
        int x = 3 + 4;
    }

    void testCtBlock() {
        { // <-- block start
            System.out.println("foo");
        }
    }

    void testCtBreak() {
        for(int i=0; i<10; i++) {
            if (i>3) {
                break; // <-- break statement
            }
        }
    }

    void testCtCase() {
        int x = 0;
        switch(x) {
            case 1: // <-- case statement
                System.out.println("foo");
        }
    }

    void testCtConditional() {
        System.out.println(
                1==0 ? "foo" : "bar" // <-- ternary conditional
        );
    }

    void testCtConstructorCall() {
        new Object();
    }

    void testCtContinue() {
        for(int i=0; i<10; i++) {
            if (i>3) {
                continue; // <-- continue statement
            }
        }
    }

    void testCtCtDo() {
        int x = 0;
        do {
            x=x+1;
        } while (x<10);
    }

    void testCtExecutableReferenceExpression() {
        java.util.function.Supplier p = Object::new;
    }

    void testCtFieldRead() {
        class Foo { int field; }
        Foo x = new Foo();
        System.out.println(x.field);
    }

    void testCtFieldWrite() {
        class Foo { int field; }
        Foo x = new Foo();
        x.field = 0;
    }

    void testCtFor() {
        // a for statement
        for(int i=0; i<10; i++) {
            System.out.println("foo");
        }
    }

    void testCtForEach() {
        java.util.List l = new java.util.ArrayList();
        for(Object o : l) { // <-- foreach loop
            System.out.println(o);
        }
    }

    void testCtIf() {
        if (1==0) {
            System.out.println("foo");
        } else {
            System.out.println("bar");
        }
    }

    void testCtInvocation() {
        // invocation of method println
        // the target is "System.out"
        System.out.println("foo");
    }

    void testCtJavaDoc() {
        /**
         * Description
         * @tag a tag in the javadoc
         */
    }

    void testCtLambda() {
        java.util.List l = new java.util.ArrayList();
        l.stream().map(
                x -> { return x.toString(); } // a lambda
        );
    }

    void testCtLiteral() {
        int x = 4; // 4 is a literal
    }

    void testCtLocalVariable() {
        // defines a local variable x
        int x = 0;

        // local variable in Java 10
        var y = 0;
    }

    void testCtNewArray() {
        // inline creation of array content
        int[] x = new int[] { 0, 1, 42};
    }

    void testCtNewClass() {
        // an anonymous class creation
        Runnable r = new Runnable() {
            @Override
            public void run() {
                System.out.println("foo");
            }
        };
    }

    void testCtOperatorAssignment() {
        int x = 0;
        x *= 3; // <-- a CtOperatorAssignment
    }

    void testCtReturn() {
        Runnable r = new Runnable() {
            @Override
            public void run() {
                return; // <-- CtReturn statement
            }
        };
    }

    void testCtSuperAccess() {
        class Foo { int foo() { return 42;}};
        class Bar extends Foo {
            int foo() {
                return super.foo(); // <-- access to super
            }
        }
    }

    void testCtSwitch() {
        int x = 0;
        switch(x) { // <-- switch statement
            case 1:
                System.out.println("foo");
        }
    }

    void testCtSwitchExpression() {
        int i = 0;
        int x = switch(i) { // <-- switch expression
            case 1 -> 10;
            case 2 -> 20;
            default -> 30;
        };
    }

    void testCtSynchronized() {
        java.util.List l = new java.util.ArrayList();
        synchronized(l) {
            System.out.println("foo");
        }
    }

    void testCtThisAccess() {
        class Foo {
            int value = 42;
            int foo() {
                return this.value; // <-- access to this
            }
        };
    }

    void testCtThrow() {
        throw new RuntimeException("oops");
    }

    void testCtTry() {
        try {
            System.out.println("foo");
        } catch (Exception ignore) {}
    }

    void testCtTryWithResource() throws IOException {
        // br is the resource
        try (java.io.BufferedReader br = new java.io.BufferedReader(new java.io.FileReader("/foo"))) {
            br.readLine();
        }
    }

    void testCtTypeAccess() throws ClassNotFoundException {
        // access to static field
        java.io.PrintStream ps = System.out;


        // call to static method
        Class.forName("Foo");


        // method reference
        java.util.function.Supplier p =
                Object::new;


        // instanceof test
        boolean x = new Object() instanceof Integer; // Integer is represented as an access to type Integer


        // fake field "class"
        Class y = Number.class;
    }

    void testCtUnaryOperator() {
        int x=3;
        --x; // <-- unary --
    }

    void testCtVariableRead() {
        String variable = "";
        System.out.println(
                variable // <-- a variable read
        );
    }

    void testCtVariableWrite() {
        String variable = "";
        variable = "new value"; // variable write


        String var = "";
        variable += "";
    }

    void testCtWhile() {
        int x = 0;
        while (x!=10) {
            x=x+1;
        };
    }

    void testCtYieldStatement() {
        int x = 0;
        x = switch ("foo") {
            default -> {
                x=x+1;
                yield x; //<--- yield statement
            }
        };


        int y = 0;
        y = switch ("foo") {
            default -> 4; //<---  implicit yield statement
        };
    }

    void testCtAnnotation() {
        // statement annotated by annotation @SuppressWarnings
        @SuppressWarnings("unchecked")
        java.util.List<?> x = new java.util.ArrayList<>();
    }

    void testCtClass() {
        // a class definition
        class Foo {
            int x;
        }
    }
}
