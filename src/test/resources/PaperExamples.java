import java.io.IOException;
import java.util.ArrayList;

public class PaperExamples {
    void figure1() {
        try { throw new ExceptionA(); }
        catch (ExceptionB) {
            System.out.println("Exception A is caught");}
    }

    void figure2(int i) {
        try {
            try {
                if (i == 0) {
                    throw new IOException();
                } else {
                    throw new ClassNotFoundException();
                }
            } finally {
                System.out.println("Internal final");
            }
        } catch (IOException e) {
            System.out.println("Caught IOException");
        } finally {
            System.out.println("External final");
        }
    }

    void figure3(int i) {
        try {
            excCaught();
            excNotCaught();
        } catch (IOException e) {
            System.out.println("Caught IOException");
        } finally {
            System.out.println("Entered final");
        }
    }

    void figure3Reversed(int i) {
        try {
            excNotCaught();
            excCaught();
        } catch (IOException e) {
            System.out.println("Caught IOException");
        } finally {
            System.out.println("Entered final");
        }
    }

    void excNotCaught() throws TypeNotPresentException {  }
    void excCaught() throws IOException {  }

    void methodThrowsExc() throws TypeNotPresentException {  }


    void figure4Original(ArrayList<Integer> list) {
        for (int i : list) {
            if (i == 0) break;
            if (i > 0) continue;
            System.out.println(i);
        }
    }

    void figure4Transformed(ArrayList<Integer> list) {
        for (int cnt = 0; cnt < list.size(); cnt++) {
            int i;
            i = list.get(cnt);
            if (i == 0) break;
            if (i > 0) continue;
            System.out.println(i);
        }
    }
}


