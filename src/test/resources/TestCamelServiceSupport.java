/**
 * This code was taken from Apache Camel project
 * https://github.com/apache/camel/blob/992a6b9685f4db49236e540af2546548cf99a7d3/core/camel-api/src/main/java/org/apache/camel/support/service/BaseService.java
 * It was slightly modified and is used solely for the testing purposes
 */

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class ServiceSupport {

    protected static final String NEW = "NEW";
    protected static final String BUILDED = "BUILDED";
    protected static final String INITIALIZED = "INITIALIZED";
    protected static final String STARTING = "STARTING";
    protected static final String STARTED = "STARTED";
    protected static final String SUSPENDING = "SUSPENDING";
    protected static final String SUSPENDED = "SUSPENDED";
    protected static final String STOPPING = "STOPPING";
    protected static final String STOPPED = "STOPPED";
    protected static final String SHUTTINGDOWN = "SHUTTINGDOWN";
    protected static final String SHUTDOWN = "SHUTDOWN";
    protected static final String FAILED = "FAILED";

    protected final Logger log = LoggerFactory.getLogger(getClass());
    protected final Object lock = new Object();
    protected volatile String status = NEW;

    public void build() {
        if (status == NEW) {
            synchronized (lock) {
                if (status == NEW) {
                    log.trace("Building service");
                    try {
                        doBuild();
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                    status = BUILDED;
                }
            }
        }
    }

    public void init() {
        if (status == NEW || status == BUILDED) {
            synchronized (lock) {
                if (status == NEW || status == BUILDED) {
                    log.trace("Initializing service: {}", this);
                    try {
                        doInit();
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                    status = INITIALIZED;
                }
            }
        }
    }

    public void start() {
        synchronized (lock) {
            if (status == STARTED) {
                log.trace("Service: {} already started", this);
                return;
            }
            if (status == STARTING) {
                log.trace("Service: {} already starting", this);
                return;
            }
            try {
                init();
            } catch (Exception e) {
                status = FAILED;
                log.trace("Error while initializing service: " + this, e);
                throw e;
            }
            try {
                status = STARTING;
                log.trace("Starting service: {}", this);
                doStart();
                status = STARTED;
                log.trace("Service started: {}", this);
            } catch (Exception e) {
                // need to stop as some resources may have been started during startup
                try {
                    stop();
                } catch (Exception e2) {
                    // ignore
                    log.trace("Error while stopping service after it failed to start: " + this + ". This exception is ignored", e);
                }
                status = FAILED;
                log.trace("Error while starting service: " + this, e);
                throw new RuntimeException(e);
            }
        }
    }

    public void stop() {
        synchronized (lock) {
            if (status == FAILED) {
                log.trace("Service: {} failed and regarded as already stopped", this);
                return;
            }
            if (status == STOPPED || status == SHUTTINGDOWN || status == SHUTDOWN) {
                log.trace("Service: {} already stopped", this);
                return;
            }
            if (status == STOPPING) {
                log.trace("Service: {} already stopping", this);
                return;
            }
            status = STOPPING;
            log.trace("Stopping service: {}", this);
            try {
                doStop();
                status = STOPPED;
                log.trace("Service: {} stopped service", this);
            } catch (Exception e) {
                status = FAILED;
                log.trace("Error while stopping service: " + this, e);
                throw new RuntimeException(e);
            }
        }
    }

    public void suspend() {
        synchronized (lock) {
            if (status == SUSPENDED) {
                log.trace("Service: {} already suspended", this);
                return;
            }
            if (status == SUSPENDING) {
                log.trace("Service: {} already suspending", this);
                return;
            }
            status = SUSPENDING;
            log.trace("Suspending service: {}", this);
            try {
                doSuspend();
                status = SUSPENDED;
                log.trace("Service suspended: {}", this);
            } catch (Exception e) {
                status = FAILED;
                log.trace("Error while suspending service: " + this, e);
                throw new RuntimeException(e);
            }
        }
    }

    public void resume() {
        synchronized (lock) {
            if (status != SUSPENDED) {
                log.trace("Service is not suspended: {}", this);
                return;
            }
            status = STARTING;
            log.trace("Resuming service: {}", this);
            try {
                doResume();
                status = STARTED;
                log.trace("Service resumed: {}", this);
            } catch (Exception e) {
                status = FAILED;
                log.trace("Error while resuming service: " + this, e);
                throw new RuntimeException(e);
            }
        }
    }

    public void shutdown() {
        synchronized (lock) {
            if (status == SHUTDOWN) {
                log.trace("Service: {} already shut down", this);
                return;
            }
            if (status == SHUTTINGDOWN) {
                log.trace("Service: {} already shutting down", this);
                return;
            }
            stop();
            status = SHUTDOWN;
            log.trace("Shutting down service: {}", this);
            try {
                doShutdown();
                log.trace("Service: {} shut down", this);
                status = SHUTDOWN;
            } catch (Exception e) {
                status = FAILED;
                log.trace("Error shutting down service: " + this, e);
                throw new RuntimeException(e);
            }
        }
    }

    public boolean isNew() {
        return status == NEW;
    }

    public boolean isBuild() {
        return status == BUILDED;
    }

    public boolean isInit() {
        return status == INITIALIZED;
    }

    public boolean isStarted() {
        return status == STARTED;
    }

    public boolean isStarting() {
        return status == STARTING;
    }

    public boolean isStopping() {
        return status == STOPPING;
    }

    public boolean isStopped() {
        return status == NEW || status == INITIALIZED || status == BUILDED || status == STOPPED || status == SHUTTINGDOWN || status == SHUTDOWN || status == FAILED;
    }

    public boolean isSuspending() {
        return status == SUSPENDING;
    }

    public boolean isSuspended() {
        return status == SUSPENDED;
    }

    public boolean isRunAllowed() {
        return isStartingOrStarted() || isSuspendingOrSuspended();
    }

    public boolean isShutdown() {
        return status == SHUTDOWN;
    }

    public boolean isStoppingOrStopped() {
        return isStopping() || isStopped();
    }

    public boolean isSuspendingOrSuspended() {
        return isSuspending() || isSuspended();
    }

    public boolean isStartingOrStarted() {
        return isStarting() || isStarted();
    }

    protected void doBuild() throws Exception {
    }

    protected void doInit() throws Exception {
    }

    protected abstract void doStart() throws Exception;

    protected abstract void doStop() throws Exception;

    protected void doSuspend() throws Exception {
    }

    protected void doResume() throws Exception {
    }

    protected void doShutdown() throws Exception {
    }
}