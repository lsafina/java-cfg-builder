import java.io.IOException;

public class TestTryCatch {
    void testEnclosedTry() {
        try {
            System.out.println("First try");
            try {
                System.out.println("Second try");
                throw new IOException();
            } catch (TypeNotPresentException e) {
                System.out.println("Ignored");
            } finally {
                System.out.println("First final");
            }
            System.out.println("Ignored");
        } catch (IOException e) {
            System.out.println("Second catcher");
        } finally {
            System.out.println("Second final");
        }
    }

    void testEnclosedTryChoiceExternal() {
        try {
            System.out.println("First try");
            if (true) {
                System.out.println("IF");
                try {
                    System.out.println("Second try");
                    throw new IOException();
                } catch (TypeNotPresentException e) {
                    System.out.println("Ignored");
                } finally {
                    System.out.println("First final");
                }
                System.out.println("Ignored");
            } else {
                System.out.println("ELSE");
            }
        } catch (IOException e) {
            System.out.println("Second catcher");
        } finally {
            System.out.println("Second final");
        }
    }

    void testEnclosedTryChoiceInternalPos() {
        try {
            System.out.println("First try");
            if (true) {
                System.out.println("IF");
                try {
                    System.out.println("Second try");
                    if (true) {
                        System.out.println("IF");
                        throw new IOException();
                    } else {
                        System.out.println("ELSE");
                    }
                } catch (TypeNotPresentException e) {
                    System.out.println("Ignored");
                } finally {
                    System.out.println("First final");
                }
            } else {
                System.out.println("ELSE");
            }
            System.out.println("Ignored");
        } catch (
                IOException e) {
            System.out.println("Second catcher");
        } finally {
            System.out.println("Second final");
        }
    }

    void testEnclosedTryChoiceInternalNeg() {
        try {
            System.out.println("First try");
            if (true) {
                System.out.println("IF");
                try {
                    System.out.println("Second try");
                    if (true) {
                        throw new IOException();
                    } else {
                        throw new ClassNotFoundException();
                    }
                    System.out.println("Ignored");
                } catch (TypeNotPresentException e) {
                    System.out.println("Caught ex TypeNotPresent");
                } finally {
                    System.out.println("First final");
                }
            } else {
                System.out.println("ELSE");
            }
            System.out.println("Only if ELSE is fired");
        } catch (
                IOException e) {
            System.out.println("Second catcher");
        } finally {
            System.out.println("Second final");
        }
    }


    void testEnclosedTryChoiceBoth() {
        try {
            System.out.println("First try");
            if (true) {
                System.out.println("EXT IF");
                try {
                    System.out.println("Second try");
                    if (true) {
                        System.out.println("INT IF");
                        throw new IOException();
                    } else {
                        System.out.println("INT ELSE");
                    }
                } catch (TypeNotPresentException e) {
                    System.out.println("Ignored");
                } finally {
                    System.out.println("First final");
                }
            } else {
                System.out.println("EXT ELSE");
            }
            System.out.println("Ignored");
        } catch (IOException e) {
            System.out.println("Second catcher");
        } finally {
            System.out.println("Second final");
        }
    }

    void tryCatchLong() {
        int i = 0;
        try {
            if (i > 0) {
                try {
                    methodThrowsExc();
                    if (i == 0) {
                        throw new IOException();
                    } else {
                        throw new ClassNotFoundException();
                    }
                    System.out.println("Ignored");
                } catch (TypeNotPresentException e) {
                    System.out.println("Caught TypeNotPresentException");
                } finally {
                    System.out.println("internal final");
                }
            }
        } catch (IOException e) {
            System.out.println("Caught IOException");
        } finally {
            System.out.println("External final");
        }
    }

    void tryCatchFinalFail(int i) {
        try {
            if (i > 0) {
                throw new IOException();
            }
        } catch (IOException e) {
            System.out.println("Caught IOException");
        } finally {
            throw new RuntimeException();
        }
    }
}


