import java.io.IOException;
import java.util.ArrayList;

public class CFGRandomTest {
    String t = "";

    void testEmptyMethod() {
        //
    }

    void testConditionEmptyBranches() {
        int i = 0;
        if (i > 0) {
        } else {
        }
    }

    void testThrowAndFor() {
        for (i = 0; i < 5; i) {
            throw new IOException();
        }
    }

    void testEnclosedLoops() {
        ArrayList<Integer> ints = new ArrayList<Integer>();
        for (int ii : ints) {
            for (int j = 0; j <= ints.size(); j) {
                System.out.println(ii);
            }
        }
    }

    void testBinaryOp() {
        int i = 0;
        if (i > 0) {
            System.out.println(i + 1);
            System.out.println(i - 1);
        } else {
            System.out.println(i * 1);
            System.out.println(i / 1);
        }
    }

    public void testChoiceThrowNormal() {
        int i = 0;
        if (i > 0) {
            throw new IOException();
        } else {
            System.out.println(i - 1);
        }
    }

    void testChoiceNormalThrow() {
        int i = 0;
        if (i > 0) {
            System.out.println(i - 1);
        } else {
            throw new IOException();
        }
    }

    public void testChoiceThrow() {
        int i = 0;
        if (i > 0) {
            throw new IOException();
        } else {
            throw new IOException();
        }
    }

    int testCondition() {
        int i = 0;
        boolean b = true;
        if (b) {
            i += 1;
        } else i -= 1;
        return i;
    }

    int testConditionThenOnly() {
        int i = 0;
        boolean b = true;
        if (b) {
            i += 1;
        }
        return i;
    }

    int testConditionElif() {
        int i = 0;
        boolean b = true;
        if (b) {
            i += 1;
        } else if (!b) {
            i += 2;
        } else {
            i += 3;
        }
        return i;
    }

    int testForInt() {
        int j = 0;
        for (int i = 0; i < 10; i++) {
            j++;
        }
        return j;
    }

    int testForEmpty() {
        int j = 0;
        for (int i = 0; i < 10; i++) {
        }
        return j;
    }

    int testwhile() {
        int i = 0;
        while (i != 10) {
            i++;
        }
        return i;
    }

    int testwhileEmpty() {
        int i = 0;
        while (i != 10) {
        }
        return i;
    }

    Boolean testDoWhile() {
        Boolean b = true;
        do {
            int i = 0;
            i += 1;
        } while (b != true);
        return b;
    }

    Boolean testDoWhileEmpty() {
        Boolean b = true;
        do {
            //nothing
        } while (b != true);
        return b;
    }

    int testSwitchPlus() {
        Boolean b = false;
        int i = 0;
        switch (i) {
            case 0: {
                i += 1;
                break;
            }
            case 1: {
                i += 2;
            }
            break;
            default:
                i += 3;
                break;
        }
        return i;
    }

    void testSwitchAss() {
        Boolean b = true;
        int i = 0;
        switch (i) {
            case 4:
                int i = 1;
                break;
            case 5:
                int i = 1;
                b = false;
                break;
            case 0: {
                b = false;
                i = 1;
            }
            case 1:
                b = false;
            case 2: {
                b = false
            }
            case 3: {
                b = false;
                break
            }
            default:
                ;
        }
    }

    void voidTest() {
        Boolean b = true;
        Integer i = 0;
    }

    void testTryCatch() {
        Integer status = 1;
        try {
            log.trace("Initializing service: {}", this);
            doBuild();
        } catch (Exception e) {
            status = 0;
            throw RuntimeCamelException.wrapRuntimeException(e);
        }
        status = 2;
    }

    void testSwitchIncr() {
        int i = 0;
        switch (i) {
            case 0: {
                i++;
                break;
            }
            case 1: {
                i++;
                break;
            }
            case 2: {
                i++;
                break;
            }
            case 3: {
                i++;
                break;
            }
            default: {
                i++;
                break;
            }
        }
    }

    void testEnclosed(){
        for (int i = 0; i< 100; i++ ){
            int j = 0;
            while (i!= 50){
                if (i + j / 2 == 0){
                    System.out.println("even");
                } else if (i - j > 0){
                    throw Exception();
                } else {
                    try {
                        ArrayList<Int> list = ArrayList(1,2,3);

                            do {
                                int k = i * j;
                            } while (k < 30)

                        throw new IOException();
                        System.out.println("Unreachable!");
                    } catch (Exception e){
                        System.out.println("Caught!");
                    } finally {
                        System.out.println("Finally!");
                    }
                }
            }
            System.out.println("Out of While!");
        }
        System.out.println("Out of FOR!");
    }
}
